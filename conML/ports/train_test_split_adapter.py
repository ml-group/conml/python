#  C O N S T R U C T I V I S T__ __
#   _____ ____   ____   /  |/  // /
#  / ___// __ \ / __ \ / /|_/ // /
# / /__ / /_/ // / / // /  / // /___
# \___/ \____//_/ /_//_/  /_//_____/
#  M A C H I N E   L E A R N I N G
#
#
# A Project by
# Thomas Schmid | UNIVERSITÄT LEIPZIG
# www.constructivist.ml
#
# Code Author: Dmitrij Denisenko
# Licence: MIT


"""Adapter to a train_test_split method

"""


from sklearn.model_selection import train_test_split


def split(learnblock, test_size):
    train, test = train_test_split(learnblock.indices(), stratify=learnblock.get_column('Z'), test_size=test_size)
    return (
        learnblock.new_block_from(train, index=True),
        learnblock.new_block_from(test, index=True)
    )