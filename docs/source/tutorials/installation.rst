.. _installation:

Installation of cml
===================

This part of documentation covers the installation of cml.


$ Install with PIP
-----------------

To install conML, run this command in your terminal::

    $ pip install conML
