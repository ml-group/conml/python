.. _guides::

This part of documentation covers answers specifc questions that the user may have.

Where do I find the application config folder?
----------------------------------------------
The application folder is located in $HOME/.conML.


Where do I find find the settings files?
----------------------------------------
The settings file is located in $HOME/.conML/settings.ini.


How to read a settings files?
-----------------------------
Call the ``conML.load_settings`` with the destination path of the settings
file.
    >>> settings = conML.load_settings("settings.ini")


How to create a constructor?
----------------------------
Import the desired unsupervised machine
learning models.
    >>> from sklearn.cluster import KMeans
    >>> from sklearn.cluster import AgglomerativeClustering

Instantiate the models and put them in a list with an abbreviation.
Both must be stored as tuples.
    >>> unsup_models = [("Kme", Kmeans()), ("Agg", AgglormerativeClustering())]

Call the function ``conML.construction`` with the the defined list and
construction type. Currently only ``conceptual`` constructors are supported.
    >>> constructor = conML.construction("conceptual", unsup_models)

How to modify the behaviour of a constructor?
---------------------------------------------
Every properly instantiated ``constructor`` object can be modified via
the properties ``min_category_size`` and ``max_categories``.
    >>> constructor.min_category_size = 0.2
    >>> constructor.max_categories = 3

How to create a reconstructor?
------------------------------

Import the desired supervised machine learning
models.
    >>> from sklearn.svm import SVC
    >>> from sklearn.neighbors import KNeighborsClassifier

Instantiate the models and put them in
a list with an abbreviation as tuple.
    >>> sup_models = [("Svc", SVC()), ("Kne", KNeighborsClassifier())]

Call the ``conML.reconstruction`` function with the ``sup_models`` as argument
and the reconstruction type ``conceptual``.
    >>> reconstruction = conML.reconstruction("conceptual", sup_models)


How to modify the behaviour of a reconstructor?
-----------------------------------------------
Every properly instantiated ``reconstructor`` object can be modified via
the properties ``min_test_accuracy`` and ``reliability_sample``.
    >>> reconstructor.min_test_accuracy = 0.7
    >>> reconstructor.reliability_sample = 0.6

How to create a deconstructor?
------------------------------
First create a reconstructor and pass them to the
function ``conML.deconstruction``.
    >>> from sklearn.svm import SVC
    >>> from sklearn.neighbors import KNeighborsClassifier
    >>> sup_models = [("Svc", SVC()), ("Kne", KNeighborsClassifier())]
    >>> reconstructor = conML.reconstruction("conceptual", sup_models)
    >>> deconstructor = conML.deconstruction(reconstructor)


How to modify the behaviour of a deconstructor?
-----------------------------------------------
Every properly instantiated ``reconstructor`` object can be modified via the
properties ``deconst_mode``, ``deconst_strategy``, ``deconst_max_distance_t``,
``deconst_full_tolerance``, ``force_time_expansion``,  ``allow_weak_reliability``
``learn_block_minimum`` and ``min_reliability``.
    >>> deconstructor.deconst_mode = "full"
    >>> deconstructor.deconst_strategy = "conservative"
    >>> deconstructor.deconst_max_distance_t = 0.01
    >>> deconstructor.deconst_full_tolerance =  0.1
    >>> deconstructor.force_time_expansion = False
    >>> deconstructor.allow_weak_reliability = False
    >>> deconstructor.learn_block_minumum = 2000
    >>> deconstructor.min_reliability = 0.1


How to load a knowledge database?
---------------------------------
You can load a database by using ``conML.load_knowledge`` function.
The only argument that is required is the path to the saved database.
    >>> database = conML.load_database("knowledge.bin")

How to save a knowledge database?
---------------------------------
The result of every knowledge search is a ``KnowledgeDatabase``.
You should use the ``save`` method of the ``KnowledgeDatabase`` object.
    >>> database.save("knowledge.bin")

How to continue a knowledge search?
-----------------------------------
If you want to continue the knowledge search, you usually start it like any
other knowledge search, but with the difference that you pass a
knowledge ``database`` when you call
the context manager.
    >>> database = conML.load_database("knowledge.bin")
    >>> with conML.knowledge_searcher(*components, database=database) as searcher:
    >>>     for block in generate_block():
    >>>         database, halde = searcher.search(block)

How to perform the knowledge search in parallel?
------------------------------------------------
If you want to perform the knowledge search in parallel, you usually start it like
any other knowledge search, but with the difference that you pass a value
bigger than 1 to ``n_procs``.
    >>> with conML.knowledge_searcher(*components, n_procs=2):
    >>>     for block in generate_block():
    >>>         database, halde = searcher.search(block)

Where do I find the log files?
------------------------------
The log files are located in $HOME/.conML.
The log folders have a timestamp as name.
The timestamp has the format day, month, year, hour, minute, seconds,
for example 31_03_2020_22_53_05.



