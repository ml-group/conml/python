.. _construction:

Construction
------------

The learning process begins with the identification of the purpose (target values) for the given learning block.
This will be done by unsupervised learning or cluster analysis.
It is the obvious analogy to the didactically formulated concept of construction, by which general creativity, innovation,
production, the search for new variations, combinations or transfers are understood.
The Bloomian taxonomy divides this knowledge as knowledge of classification,
categories and structures and introduces it under the term conceptual knowledge.

The user controls which clustering methods are used and is responsible for the configuration of their hyperparameters.
Algorithms that require as input, the number of clusters to be determined, are iteratively tested with a different
number of clusters. The maximum number of constellations to be tested is specified by the user.
Which procedure seems to be most suitable for the construction and meets the requirements of reconstruction and deconstruction is a priori undetermined.

Thus, as many different machine models as possible are formed from a single learning block, reconstructed and then ranked in
descending order of their interrater reliability. The highest ranked model is considered the winner and is fed directly into the deconstruction.
However, each clustering result is first checked for validity. If a cluster consists of too few samples,
the result is considered invalid. In this case, the next constellation must be used.
The construction thus represents nothing more than a multiplication or diversification
mechanism in which clustering procedures are applied to a learning block.

.. image:: images/construction_process.png
  :width: 400
  :alt: Alternative text
  :align: center

