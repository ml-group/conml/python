.. _conml::
Constructivst Machine Learning
------------------------------

Constructivist machine learning is the interpretation of learning on the basis
of so-called constructivism, which views learning as the creation of an individual
representation of the world determined by the behaviour, thinking and actions
of a subject or the creation of collective perception through social interaction.

For more information use the following `LINK
<https://nbn-resolving.org/urn:nbn:de:bsz:15-qucosa2-318388>`_.
