.. _api:

This part of the documentation covers all classes, methods and functions.

API
---
.. automodule:: cml.controller.api
.. autofunction:: cml.controller.api.load_settings
.. autofunction:: cml.controller.api.get_settings
.. autofunction:: cml.controller.api.load_knowledge
.. autofunction:: cml.controller.api.construction
.. autofunction:: cml.controller.api.feature_selection
.. autofunction:: cml.controller.api.reconstruction
.. autofunction:: cml.controller.api.deconstruction
.. autofunction:: cml.controller.api.knowledge_searcher

Domain
------

Construction
***********
.. _identification:
.. automodule:: cml.domain.construction.construction
.. autoclass:: cml.domain.construction.construction.Constructor
    :members:

Reconstruction
************
.. automodule:: cml.domain.reconstruction.reconstruction
.. autoclass:: cml.domain.reconstruction.reconstruction.Reconstructor
    :members:

.. automodule:: cml.domain.reconstruction.selection
.. autoclass:: cml.domain.reconstruction.selection.FeatureSelector
    :members:

Deconstruction
**************
.. automodule:: cml.domain.deconstruction.deconstruction
.. autoclass:: deconstruction.deconstruction.Deconstructor
    :members:

.. automodule:: cml.domain.deconstruction.abstract
.. autoclass:: cml.domain.deconstruction.abstract.Abstract
    :members:

.. automodule:: cml.domain.deconstruction.complete
.. autoclass:: cml.domain.deconstruction.complete.CompleteDeconstructor
    :members:

.. automodule:: cml.domain.deconstruction.t_sigma
.. autoclass:: cml.domain.deconstruction.t_sigma.TSigmaDeconstructor
    :members:

.. automodule:: cml.domain.deconstruction.z_sigma
.. autoclass:: cml.domain.deconstruction.z_sigma.ZSigmaDeconstructor
    :members:


Knowledge database
******************
.. automodule:: cml.domain.knowledge
    :members:

Lernblock identification
************************

.. automodule:: cml.domain.preprocessing
.. autoclass:: cml.domain.preprocessing.Source
    :members:
.. autoclass:: cml.domain.preprocessing.Learnblock
    :members:
.. autoclass:: cml.domain.preprocessing.ClusterFinder
    :members:


Usecases
--------

.. automodule:: cml.usecases.usecase
.. autoclass:: cml.usecases.command.ModelCreationMixin
    :members:
.. autoclass:: cml.usecases.command.FeatureSelectionUsecase
    :members:
.. autoclass:: cml.usecases.command.ReconstructionUsecase
    :members:
.. autoclass:: cml.usecases.command.ConstructionUsecase
    :members:
.. autoclass:: cml.usecases.command.DeconstructionUsecase
    :members:

.. automodule:: cml.usecases.query
.. autoclass:: cml.usecases.query.ParallelRunner
    :members:

.. autofunction:: cml.usecases.query.map_onto

.. autoclass:: cml.usecases.query.KnowledgeSearcher
    :members:

.. autoclass:: cml.usecases.KnowledgeSearcherUsecase
    :members:

Ports
-----

scikit-learn Adapter
********************

.. automodule:: cml.ports.ml_adapter
.. autoclass:: cml.ports.ml_adapter.MachineLearningModel
    :members:
.. autoclass:: cml.ports.ml_adapter.FilterMethod
    :members:
.. autoclass:: cml.ports.EmbeddedMethod
    :members:
.. autoclass:: cml.ports.ConstructionClusteringMLModel
    :members:
.. autoclass:: cml.ports.ReconstructionConceptualMLModel
    :members:
.. autoclass:: cml.ports.KernelDensityEstimator

pandas Adapter
**************

.. automodule:: cml.ports.source_adapter
.. autoclass:: cml.ports.source_adapter.PandasBlock
    :members:
.. autofunction:: convert_df_to_block
.. autofunction:: build_block_from_rows
.. autofunction:: build_new_learnblock

ckmeans Adapter
***************

.. automodule:: cml.ports.ckmeans_adapter
.. autofunction:: cml.ports.ckmeans_adapter.ckmeans

krippendorff Adapter
********************

.. automodule:: cml.ports.krippendorff_adapter
.. autofunction:: cml.ports.krippendorff_adapter.krippendorff_alpha

Shared
------

Exceptions
**********

.. automodule:: cml.shared.errors
.. autoclass:: cml.shard.errors.BaseError
.. autoclass:: cml.shard.errors.ModeError
.. autoclass:: cml.shard.errors.IndexOutOfError
.. autoclass:: cml.shard.errors.ConfigurationFileError


Logging
*******

.. automodule:: cml.shared.logger
.. autoclass:: cml.shared.logger.LogMeMixin
    :members:
.. autoclass:: cml.shared.logger.BlockProcessingInfo
    :members:
.. autoclass:: cml.shared.logger.LernblockIdentificationInfo
    :members:
.. autoclass:: cml.shared.logger.HighLevelLearnblockInfo
    :members:
.. autoclass:: cml.shared.logger.ConstructionInfo
    :members:
.. autoclass:: cml.shared.logger.SelectionInfo
    :members:
.. autoclass:: cml.shared.logger.ReconstructionInfo
    :members:
.. autoclass:: cml.shared.logger.WinnerSelectionInfo
    :members:
.. autoclass:: cml.shared.logger.DeconstructionInfo
    :members:
.. autoclass:: cml.shared.logger.JsonContainer
    :members:

Settings
********

.. automodule:: cml.shared.settings
.. autoclass:: cml.shared.settings.MetaSettings
.. autoclass:: cml.shared.settings.GeneralSettings
.. autoclass:: cml.shared.settings.BlockProcessingSettings
.. autoclass:: cml.shared.settings.ConstructionSettings
.. autoclass:: cml.shared.settings.FeatureSelectionSettings
.. autoclass:: cml.shared.settings.ReconstructionSettings
.. autoclass:: cml.shared.settings.DeconstructionSettings
.. autofunction:: cml.shared.settings.specific_settings_factory
.. autofunction:: cml.shared.settings.read_settings
.. autofunction:: cml.shared.settings.configure_main_settings_class

Parameter
*********

.. automodule:: cml.shared.parameter
.. autoclass:: cml.shared.parameter.HighestLevel
.. autoclass:: cml.shared.parameter.KernelBandwidth
.. autoclass:: cml.shared.parameter.LearnblockMinimum
.. autoclass:: cml.shared.parameter.MaxCategories
.. autoclass:: cml.shared.parameter.MinCategorySize
.. autoclass:: cml.shared.parameter.SigmaZetaCutoff
.. autoclass:: cml.shared.parameter.MaxModelTargets
.. autoclass:: cml.shared.parameter.MaxTargetError
.. autoclass:: cml.shared.parameter.MaxFeatures
.. autoclass:: cml.shared.parameter.MaxFilterX
.. autoclass:: cml.shared.parameter.MaxFilterY
.. autoclass:: cml.shared.parameter.MaxModelsReduction
.. autoclass:: cml.shared.parameter.MinTestAccuracy
.. autoclass:: cml.shared.parameter.MinBuildModels
.. autoclass:: cml.shared.parameter.ReduceModelRedundancy
.. autoclass:: cml.shared.parameter.DeconstStrategy
.. autoclass:: cml.shared.parameter.DeconstMode
.. autoclass:: cml.shared.parameter.DeconstMaxDistanceT
.. autoclass:: cml.shared.parameter.DeconstFullTolerance
.. autoclass:: cml.shared.parameter.ForceTimeExpansion
.. autoclass:: cml.shared.parameter.AllowWeakReliability
