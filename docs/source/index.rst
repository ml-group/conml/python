.. cml documentation master file, created by
   sphinx-quickstart on Fri Apr  3 21:45:45 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

cml - constructivst machine learning
====================================

Tutorials
---------

.. toctree::
   :maxdepth: 2

   tutorials/installation
   tutorials/quickstart


How-To Guides
-------------

.. toctree::
   :maxdepth: 2

   guides/guides


Reference
---------
.. toctree::
   :maxdepth: 2

   reference/api


Explanation
-----------

.. toctree::
   :maxdepth: 2

   explanations/conml
   explanations/construction
   explanations/reconstruction
   explanations/deconstruction
