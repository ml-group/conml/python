from copy import deepcopy
import datetime
from time import time

import pytest
from collections import deque
from unittest.mock import MagicMock, patch
import pandas as pd
from sklearn.svm import SVC
from sklearn.tree import DecisionTreeClassifier

import conML
from conML.domain.deconstruction.abstract import Abstract
from conML.domain.reconstruction import PragmaticMachineLearningModel, Metadata
from conML.domain.knowledge import KnowledgeDatabase, RelativeFinder
from conML.ports.source_adapter import PandasBlock, build_block_from_rows
from conML.shared.logger import DeconstructionInfo


def create_deconstructor():
    algorithms = [
        ("Svc", SVC(gamma="scale")),
        ("Tree", DecisionTreeClassifier())
    ]
    reconstructor = conML.reconstruction("conceptual", algorithms)
    return conML.deconstruction(reconstructor)


def get_labeled_block(df):
    labeled = PandasBlock(df, relationship=("Z", "Sigma"), origin=("source", ))
    labeled.subject = "Kme02"
    labeled.n_cluster = 2
    labeled.origin = ("C.1.1", "C.1.2")
    return labeled


def create_pmlm():
    df = pd.DataFrame(
        {
            "0.0.1": [pow(i, 2) for i in range(10, 100)],
            "0.0.2": [pow(i, 0.5) for i in range(10, 100)],
            "0.0.3": [pow(i, 3) for i in range(10, 100)],
            "0.0.4": [pow(i, 0.3) for i in range(10, 100)],
            "0.0.5": [pow(i, 0.1) for i in range(10, 100)],
            "0.0.6": [pow(i, 4) for i in range(10, 100)],
            "T": [int(time()) for i in range(10, 100)],
            "Sigma": ["" for i in range(10, 100)],
            "Z": [0 if i < 50 else 1 for i in range(10, 100)]
        }
    )

    meta_data = Metadata(
        knowledge_domain="C",
        knowledge_level=2,
        identifier=7,
        learnblock_indices=None,
        learnblock_labels=df["Z"],
        learnblock_features=list(df.columns),
        t_min=min(df["T"]),
        t_max=max(df["T"]),
        subject=("Svc",),
        aim=("Kme02",)
    )

    svc = SVC(gamma="scale")
    prag = PragmaticMachineLearningModel(meta_data, svc, get_labeled_block(df))
    prag.set_subjects(["Svc", "Tree"])
    return prag


def test_complete_deconstruction_with_one_relative_minimal_success():
    # arrange
    knowledge = KnowledgeDatabase(8, build_block_from_rows)
    knowledge.database[2].biggest_id = 10

    deconstructor = create_deconstructor()
    deconstructor.deconst_mode = "minimal"

    pragamtic = create_pmlm()
    relative = deepcopy(pragamtic)
    relative.identifier = 4
    relative.knowledge_level = 2
    relative._PragmaticMachineLearningModel__learnblock.origin = ("C.1.3", "C.1.4")
    knowledge.insert(relative)


    # act
    for log in deconstructor.state.run_complete_deconstruction(
        level=2, pragmatic=pragamtic, database=knowledge, relatives=[relative]):

        # assert
        inserted_model = log.inserted.pop(0)
        assert log.state == log.State.DECONSTRUCTED
        assert log.relationship == ("complete", )
        assert relative in log.deleted
        assert inserted_model in log.reconstructed_info
        assert inserted_model == knowledge.get_model(inserted_model.uid)
        assert inserted_model.uid == "C.2.11"
        assert inserted_model.subject == ("Svc", )
        assert inserted_model.aim == ("Kme02", )
        assert (set(inserted_model._PragmaticMachineLearningModel__learnblock.origin)
                == set(["C.1.1", "C.1.2", "C.1.3", "C.1.4"]))


def test_complete_deconstruction_with_many_relative_minimal():
    # arrange
    knowledge = KnowledgeDatabase(8, build_block_from_rows)
    knowledge.database[2].biggest_id = 10

    deconstructor = create_deconstructor()
    deconstructor.deconst_mode = "minimal"
    pragamtic = create_pmlm()

    relative = deepcopy(pragamtic)
    relative.identifier = 4
    relative.knowledge_level = 2
    relative._PragmaticMachineLearningModel__learnblock.origin = (
        "C.1.3", "C.1.4"
    )

    relative_2 = deepcopy(pragamtic)
    relative_2.identifier = 5
    relative_2.knowledge_level = 2
    relative_2._PragmaticMachineLearningModel__learnblock.origin = (
        "C.1.6", "C.1.7"
    )

    knowledge.insert(relative)
    knowledge.insert(relative_2)
    relatives = [relative, relative_2]

    # act
    it = iter(deconstructor.state.run_complete_deconstruction(
            level=2,
            pragmatic=pragamtic,
            database=knowledge,
            relatives=relatives
        )
    )

    # assert
    log_1 = next(it)
    assert log_1.inserted.pop().uid == "C.2.11"


def build_pragmatic_and_relative():
    pragmatic = create_pmlm()
    relative = deepcopy(pragmatic)
    relative.identifer = 4
    relative._PragmaticMachineLearningModel__learnblock.origin = ("C.1.3", "C.1.4")
    return pragmatic, relative


def create_complete_ingredients():
    from collections import deque, namedtuple
    from conML.domain.deconstruction.complete import CompleteDeconstructor

    knowledge = KnowledgeDatabase(8, build_block_from_rows)
    deconstructor = create_deconstructor()
    pragmatic, relative = build_pragmatic_and_relative()
    result_queue = deque()
    complete = CompleteDeconstructor(
        2,
        knowledge,
        deconstructor.state.settings,
        deconstructor.state.reconstructor,
        pragmatic,
        [relative],
        result_queue,
        general_settings=deconstructor.state.general_settings,
        block_processing_settings=deconstructor.state.block_processing_settings,
        cluster_finder=deconstructor.state.cluster_finder
    )

    Ingredients = namedtuple("Ingredients", ("deconstructor, result_dequeu, "
                                             "pragmatic, relative, knowledge"))

    return Ingredients(complete, result_queue, pragmatic, relative, knowledge)


class TestCompleteDeconstructor:
    from conML.shared.logger import DeconstructionInfo

    def test_reconstructing(self):
        # arrange
        ingredients = create_complete_ingredients()
        ingredients.knowledge.insert(ingredients.pragmatic)
        block = ingredients.pragmatic.trained_with(ingredients.knowledge)
        block.columns = lambda **x: [1]

        # act
        log = ingredients.deconstructor.reconstructing(
            meta=None, learnblock=block, relative=ingredients.relative,
            log=DeconstructionInfo()
        )

        # assert
        assert log.inserted == []
        assert log.deleted == [ingredients.pragmatic]
        assert log.state == DeconstructionInfo.State.FAILED

    def test_model_differentitation_with_one_time_cluster_success(self):
        # arrange
        ingredients = create_complete_ingredients()
        ingredients.knowledge.database[2].biggest_id = 7
        ingredients.knowledge.insert(ingredients.relative)
        ingredients.knowledge.insert(ingredients.pragmatic)
        block = ingredients.pragmatic.trained_with(ingredients.knowledge)
        log = self.DeconstructionInfo()

        # act
        ingredients.deconstructor.cluster_finder.find_valid_learnblock_cluster = \
            lambda *args, **kw: block.get_column("T")

        block.new_block_from = lambda *a, **kw: block

        ingredients.deconstructor.model_differentiation(
            block,
            ingredients.relative,
            log
        )

        # assert
        winner = log.inserted.pop(0)

        assert winner.uid == "C.2.9"
        assert winner.aim == ("C.2.Kme02", )
        assert winner.subject == ("Tree", )
        assert winner._PragmaticMachineLearningModel__learnblock.origin == (
            "C.1.1", "C.1.2"
        )
        assert winner._PragmaticMachineLearningModel__learnblock.subject == (
            "Kme02"
        )
        assert winner in ingredients.knowledge

    def test_model_differentitation_with_two_time_cluster_success(self):
        # arrange
        ingredients = create_complete_ingredients()
        ingredients.knowledge.database[2].biggest_id = 7
        ingredients.knowledge.insert(ingredients.relative)
        ingredients.knowledge.insert(ingredients.pragmatic)
        block = ingredients.pragmatic.trained_with(ingredients.knowledge)
        log = self.DeconstructionInfo()

        mock_find = MagicMock()
        mock_find.return_value = [
            list(map(lambda x: int(x), block.df["T"][:50].values)),
            list(map(lambda x: int(x), block.df["T"][50:].values)),
        ]
        ingredients.deconstructor.cluster_finder.find = mock_find

        # act
        ingredients.deconstructor.model_differentiation(
            block,
            ingredients.relative,
            log
        )

        # assert
        for winner in log.inserted:
            assert winner.aim == ("C.2.Kme02", )
            assert winner.subject == ("Tree", )
            assert winner._PragmaticMachineLearningModel__learnblock.origin == (
                "C.1.1", "C.1.2"
            )
            assert winner._PragmaticMachineLearningModel__learnblock.subject == (
                "Kme02"
            )
            assert winner in ingredients.knowledge

    def test_model_differentitation_delete_dependent_models(self):
        # arrange
        ingredients = create_complete_ingredients()
        ingredients.knowledge.database[2].biggest_id = 7
        block = ingredients.pragmatic.trained_with(ingredients.knowledge)
        log = self.DeconstructionInfo()
        ingredients.deconstructor.cluster_finder.find_valid_learnblock_cluster = lambda *a: []

        mock_find = MagicMock()
        mock_find.return_value = []
        ingredients.deconstructor.cluster_finder.find = mock_find

        ingredients.knowledge.insert(ingredients.pragmatic)
        ingredients.knowledge.insert(ingredients.relative)

        m_3 = create_pmlm()
        m_3.identifier = 103
        m_3.meta.knowledge_level = 3
        m_3.origin = (ingredients.relative.uid, )
        ingredients.knowledge.insert(m_3)

        m_2 = create_pmlm()
        m_2.meta.knowledge_level = 4
        m_2.identifier = 105
        m_2.origin = (m_3.uid, )
        ingredients.knowledge.insert(m_2)

        m_5 = create_pmlm()
        m_5.meta.knowledge_level = 3
        m_5.identifier = 106
        m_5.origin = ()
        ingredients.knowledge.insert(m_5)

        m_1 = create_pmlm()
        m_1.identifier = 101
        m_1.meta.knowledge_level = 4
        m_1.origin = (m_3.uid, m_5.uid)
        ingredients.knowledge.insert(m_1)

        # act
        ingredients.deconstructor.model_differentiation(
                block,
                ingredients.relative,
                log
        )

        # assert
        # assert ingredients.relative not in ingredients.knowledge
        # assert ingredients.relative in log.deleted
        assert m_3 not in ingredients.knowledge and m_3 in log.deleted
        assert m_1 not in ingredients.knowledge and m_1 in log.deleted
        assert m_2 not in ingredients.knowledge and m_2 in log.deleted
        assert m_5 in ingredients.knowledge

    def test_feature_contraints_success(self):
        # arrange
        ingredients = create_complete_ingredients()
        ingredients.knowledge.database[2].biggest_id = 7
        block = ingredients.pragmatic.trained_with(ingredients.knowledge)
        relative_block = ingredients.relative.trained_with(ingredients.knowledge)
        ingredients.deconstructor.block = block
        ingredients.deconstructor.relative_block = relative_block

        # act
        actual_response = ingredients.deconstructor.feature_constraint()

        # assert
        assert actual_response is True

    def test_feature_contraints_failure(self):
        # arrange
        ingredients = create_complete_ingredients()
        ingredients.knowledge.database[2].biggest_id = 7

        # delete some features from relative model
        block = ingredients.relative.trained_with(ingredients.knowledge)
        del block.df["0.0.1"]
        del block.df["0.0.2"]
        del block.df["0.0.3"]
        del block.df["0.0.4"]
        del block.df["0.0.5"]
        relative_block = ingredients.relative.trained_with(ingredients.knowledge)
        ingredients.deconstructor.block = block
        ingredients.deconstructor.relative_block = relative_block

        # act
        actual_response = ingredients.deconstructor.feature_constraint()

        # assert
        assert actual_response is False

    def test_reliability_contraint_success(self):
        # arrange
        ingredients = create_complete_ingredients()
        df_1 = pd.DataFrame(
            {
                "0.0.1": [0.1, 0.2, 0.3, 0.4, 0.5],
                "T": [datetime.datetime.strptime("2020-01-01 08:00",
                                                 "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime("2020-01-01 08:01",
                                                 "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime("2020-01-01 08:02",
                                                 "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime("2020-01-01 08:03",
                                                 "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime("2020-01-01 08:04",
                                                 "%Y-%m-%d %H:%M")],
                "Sigma": [" " for _ in range(5)],
                "Z": [2, 2, 1, 2, 2]
            }
        )
        block_pragmatic = get_labeled_block(df_1)

        df_2 = pd.DataFrame(
            {
                "0.0.1": [0.1, 0.2, 0.3, 0.4, 0.5],
                "T": [datetime.datetime.strptime("2020-01-01 08:00",
                                                 "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime("2020-01-01 08:01",
                                                 "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime("2020-01-01 08:02",
                                                 "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime("2020-01-01 08:03",
                                                 "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime("2020-01-01 08:04",
                                                 "%Y-%m-%d %H:%M")],
                "Sigma": [" " for _ in range(5)],
                "Z": [2, 2, 1, 2, 2]
            }
        )

        block_relative = get_labeled_block(df_2)
        ingredients.deconstructor.block = block_pragmatic
        ingredients.deconstructor.relative_block = block_relative

        # act
        actual_result = ingredients.deconstructor.reliability_constraint()

        # assert
        assert actual_result

    def test_reliability_contraint_failure(self):
        # arrange
        ingredients = create_complete_ingredients()
        df_1 = pd.DataFrame(
            {
                "0.0.1": [0.1, 0.2, 0.3, 0.4, 0.5],
                "T": [datetime.datetime.strptime("2020-01-01 08:00", "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime("2020-01-01 08:01", "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime("2020-01-01 08:02", "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime("2020-01-01 08:03", "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime("2020-01-01 08:04", "%Y-%m-%d %H:%M")],
                "Sigma": [" " for _ in range(5)],
                "Z": [2, 2, 1, 2, 2]
            }
        )
        block_pragmatic = get_labeled_block(df_1)

        df_2 = pd.DataFrame(
            {
                "0.0.1": [0.1, 0.2, 0.3, 0.4, 0.5],
                "T": [datetime.datetime.strptime("2020-01-01 08:00", "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime("2020-01-01 08:01", "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime("2020-01-01 08:02", "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime("2020-01-01 08:03", "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime("2020-01-01 08:04", "%Y-%m-%d %H:%M")],
                "Sigma": [" " for _ in range(5)],
                "Z": [2, 2, 1, 2, 1]
            }
        )

        block_relative = get_labeled_block(df_2)
        ingredients.deconstructor.block = block_pragmatic
        ingredients.deconstructor.relative_block = block_relative

        # act
        actual_result = ingredients.deconstructor.time_stamp_constraint()

        # assert
        assert actual_result is False

    def test_deconstruct_valid_reliability_constraint(self):
        # arrange
        ingredients = create_complete_ingredients()
        ingredients.deconstructor.feature_constraint = lambda *args: False
        ingredients.deconstructor.time_stamp_constraint = lambda *args: True
        ingredients.deconstructor.pragmatic = ingredients.pragmatic
        ingredients.deconstructor.relative_block = \
            ingredients.relative.trained_with(ingredients.knowledge)
        log = ingredients.deconstructor.log_info(
            ingredients.relative, ingredients.pragmatic
        )

        # act
        res = ingredients.deconstructor.deconstruct(ingredients.relative, log)

        # assert
        print(res)

    def test_build_block_from_diff_features_success_Z(self):
        # arrange
        ingredients = create_complete_ingredients()
        df_1 = pd.DataFrame(
            {
                "0.0.1": [0.1, 0.2, 0.3, 0.4, 0.5],
                "0.0.2": [0.1, 0.2, 0.3, 0.4, 0.5],
                "T": [int(datetime.datetime.strptime("2020-01-01 08:00", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:01", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:02", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:03", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:04", "%Y-%m-%d %H:%M").timestamp())],
                "Sigma": ["Kme" for _ in range(5)],
                "Z": [2, 2, 1, 2, 2]
            }
        )
        block_pragmatic = get_labeled_block(df_1)

        df_2 = pd.DataFrame(
            {
                "0.0.2": [0.1, 0.2, 0.3, 0.4, 0.5],
                "0.0.3": [0.1, 0.2, 0.3, 0.4, 0.5],
                "T": [int(datetime.datetime.strptime("2020-01-01 08:00", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:01", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:02", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:03", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:07", "%Y-%m-%d %H:%M").timestamp())],
                "Sigma": ["Kme" for _ in range(5)],
                "Z": [2, 2, 1, 2, 1]
            }
        )

        block_relative = get_labeled_block(df_2)
        block_relative.origin = ("C.1.3", "C.1.4")
        ingredients.deconstructor.block = block_pragmatic
        ingredients.deconstructor.relative_block = block_relative

        # act
        actual_result = (
            ingredients.deconstructor.build_block_from_diff_features()
        )

        # assert
        assert "0.0.3" in actual_result.columns()
        assert "0.0.1" in actual_result.columns()
        assert "Z" in actual_result.columns()
        assert "Sigma" in actual_result.columns()
        assert "Z" in actual_result.columns()
        assert actual_result.n_rows() == 4
        assert actual_result.get_column("Sigma") == ["Kme" for _ in range(4)]
        assert actual_result.subject == "Kme02"
        assert set(actual_result.origin) == set(
            block_pragmatic.origin + block_relative.origin
        )

    def test_build_block_from_diff_features_success_z_other(self):
        # arrange
        ingredients = create_complete_ingredients()
        df_1 = pd.DataFrame(
            {
                "0.0.1": [0.1, 0.2, 0.3, 0.4, 0.5, 0.6],
                "0.0.2": [0.1, 0.2, 0.3, 0.4, 0.5, 0.6],
                "T": [int(datetime.datetime.strptime("2020-01-01 08:00", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:01", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:02", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:03", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:08", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:04", "%Y-%m-%d %H:%M").timestamp())],
                "Sigma": ["Kme" for _ in range(6)],
                "Z": [2, 2, 1, 3, 7, 2]
            }
        )
        block_pragmatic = get_labeled_block(df_1)

        df_2 = pd.DataFrame(
            {
                "0.0.2": [0.1, 0.2, 0.3, 0.4, 0.5],
                "0.0.3": [0.1, 0.2, 0.3, 0.4, 0.5],
                "T": [int(datetime.datetime.strptime("2020-01-01 08:00", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:01", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:02", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:03", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:07", "%Y-%m-%d %H:%M").timestamp())],
                "Sigma": ["Kme" for _ in range(5)],
                "Z": [2, 2, 1, 2, 1]
            }
        )

        block_relative = get_labeled_block(df_2)
        block_relative.origin = ("C.1.3", "C.1.4")
        ingredients.deconstructor.block = block_pragmatic
        ingredients.deconstructor.relative_block = block_relative
        ingredients.deconstructor.settings.deconst_strategy = "integrative"

        # act
        actual_result = (
            ingredients.deconstructor.build_block_from_diff_features()
        )

        # assert
        assert "0.0.3" in actual_result.columns()
        assert "0.0.1" in actual_result.columns()
        assert "Z" in actual_result.columns()
        assert "Sigma" in actual_result.columns()
        assert "Z" in actual_result.columns()
        assert list(df_2["Z"][:-1]) == actual_result.get_column("Z")
        assert actual_result.n_rows() == 4
        assert actual_result.get_column("Sigma") == ["Kme" for _ in range(4)]
        assert actual_result.subject == "Kme02"
        assert set(actual_result.origin) == set(
            block_pragmatic.origin + block_relative.origin
        )

    def test_build_block_from_diff_features_success_oppurtunistic(self):
        # arrange
        ingredients = create_complete_ingredients()
        df_1 = pd.DataFrame(
            {
                "0.0.1": [0.1, 0.2, 0.3, 0.4, 0.5, 0.6],
                "0.0.2": [0.1, 0.2, 0.3, 0.4, 0.5, 0.6],
                "T": [int(datetime.datetime.strptime("2020-01-01 08:00", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:01", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:02", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:03", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:08", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:04", "%Y-%m-%d %H:%M").timestamp())],
                "Sigma": ["Kme" for _ in range(6)],
                "Z": [2, 2, 1, 3, 7, 2]
            }
        )
        block_pragmatic = get_labeled_block(df_1)

        df_2 = pd.DataFrame(
            {
                "0.0.2": [0.1, 0.2, 0.3, 0.4, 0.5],
                "0.0.3": [0.1, 0.2, 0.3, 0.4, 0.5],
                "T": [int(datetime.datetime.strptime("2020-01-01 08:00", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:01", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:02", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:03", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:07", "%Y-%m-%d %H:%M").timestamp())],
                "Sigma": ["Kme" for _ in range(5)],
                "Z": [2, 2, 1, 2, 1]
            }
        )

        block_relative = get_labeled_block(df_2)
        block_relative.origin = ("C.1.3", "C.1.4")
        ingredients.deconstructor.block = block_pragmatic
        ingredients.deconstructor.relative_block = block_relative
        ingredients.deconstructor.settings.deconst_strategy = "oppurtunistic"

        # act
        actual_result = (
            ingredients.deconstructor.build_block_from_diff_features()
        )

        # assert
        assert "0.0.3" in actual_result.columns()
        assert "0.0.1" in actual_result.columns()
        assert "Z" in actual_result.columns()
        assert "Sigma" in actual_result.columns()
        assert "Z" in actual_result.columns()
        assert list(df_1["Z"][:-2]) == actual_result.get_column("Z")
        assert actual_result.n_rows() == 4
        assert actual_result.get_column("Sigma") == ["Kme" for _ in range(4)]
        assert actual_result.subject == "Kme02"
        assert set(actual_result.origin) == set(
            block_pragmatic.origin + block_relative.origin
        )

    def test_build_block_from_diff_features_success_oppurtunistic_z_other(self):
        # arrange
        ingredients = create_complete_ingredients()
        df_1 = pd.DataFrame(
            {
                "0.0.1": [0.1, 0.2, 0.3, 0.4, 0.5],
                "0.0.2": [0.1, 0.2, 0.3, 0.4, 0.5],
                "T": [int(datetime.datetime.strptime("2020-01-01 08:00", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:01", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:02", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:03", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:04", "%Y-%m-%d %H:%M").timestamp())],
                "Sigma": ["Kme" for _ in range(5)],
                "Z": [2, 2, 1, 3, 7]
            }
        )
        block_pragmatic = get_labeled_block(df_1)

        df_2 = pd.DataFrame(
            {
                "0.0.2": [0.1, 0.2, 0.3, 0.4, 0.5, 0.6],
                "0.0.3": [0.1, 0.2, 0.3, 0.4, 0.5, 0.6],
                "T": [int(datetime.datetime.strptime("2020-01-01 08:00", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:01", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:02", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:03", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:08", "%Y-%m-%d %H:%M").timestamp()),
                      int(datetime.datetime.strptime("2020-01-01 08:07", "%Y-%m-%d %H:%M").timestamp())],
                "Sigma": ["Kme" for _ in range(6)],
                "Z": [2, 1, 1, 2, 1, 7]
            }
        )

        block_relative = get_labeled_block(df_2)
        block_relative.origin = ("C.1.3", "C.1.4")
        ingredients.deconstructor.block = block_pragmatic
        ingredients.deconstructor.relative_block = block_relative
        ingredients.deconstructor.settings.deconst_strategy = "oppurtunistic"

        # act
        actual_result = (
            ingredients.deconstructor.build_block_from_diff_features()
        )

        # assert
        assert "0.0.3" in actual_result.columns()
        assert "0.0.1" in actual_result.columns()
        assert "Sigma" in actual_result.columns()
        assert "Z" in actual_result.columns()
        assert list(df_1["Z"][:-1]) == actual_result.get_column("Z")
        assert actual_result.n_rows() == 4
        assert actual_result.get_column("Sigma") == ["Kme" for _ in range(4)]
        assert actual_result.subject == "Kme02"
        assert set(actual_result.origin) == set(
            block_pragmatic.origin + block_relative.origin
        )

    def test_build_block_from_diff_features_empty_block_result(self):
        # arrange
        ingredients = create_complete_ingredients()
        df_1 = pd.DataFrame(
            {
                "0.0.1": [0.1, 0.2, 0.3, 0.4, 0.5],
                "0.0.2": [0.1, 0.2, 0.3, 0.4, 0.5],
                "T": [datetime.datetime.strptime("2020-01-01 08:10", "%Y-%m-%d %H:%M").timestamp(),
                      datetime.datetime.strptime("2020-01-01 08:20", "%Y-%m-%d %H:%M").timestamp(),
                      datetime.datetime.strptime("2020-01-01 08:30", "%Y-%m-%d %H:%M").timestamp(),
                      datetime.datetime.strptime("2020-01-01 08:40", "%Y-%m-%d %H:%M").timestamp(),
                      datetime.datetime.strptime("2020-01-01 08:50", "%Y-%m-%d %H:%M").timestamp()],
                "Sigma": ["Kme" for _ in range(5)],
                "Z": [2, 2, 1, 2, 2]
            }
        )
        block_pragmatic = get_labeled_block(df_1)

        df_2 = pd.DataFrame(
            {
                "0.0.2": [0.1, 0.2, 0.3, 0.4, 0.5],
                "0.0.3": [0.1, 0.2, 0.3, 0.4, 0.5],
                "T": [datetime.datetime.strptime("2020-01-01 08:00", "%Y-%m-%d %H:%M").timestamp(),
                      datetime.datetime.strptime("2020-01-01 08:01", "%Y-%m-%d %H:%M").timestamp(),
                      datetime.datetime.strptime("2020-01-01 08:02", "%Y-%m-%d %H:%M").timestamp(),
                      datetime.datetime.strptime("2020-01-01 08:03", "%Y-%m-%d %H:%M").timestamp(),
                      datetime.datetime.strptime("2020-01-01 08:07", "%Y-%m-%d %H:%M").timestamp()],
                "Sigma": ["Kme" for _ in range(5)],
                "Z": [2, 2, 1, 2, 1]
            }
        )

        block_relative = get_labeled_block(df_2)
        ingredients.deconstructor.block = block_pragmatic
        ingredients.deconstructor.relative_block = block_relative

        # act
        actual_result = (
            ingredients.deconstructor.
            build_block_from_diff_features()
        )

        # assert
        assert "Z" in actual_result.columns()
        assert "Sigma" in actual_result.columns()
        assert "Z" in actual_result.columns()
        assert actual_result.n_rows() == 0
        assert actual_result.get_column("Sigma") == []
        assert actual_result.subject == "Kme02"
        assert set(actual_result.origin) == set(
            block_pragmatic.origin + block_relative.origin
        )

    def test_model_fusion_success(self):
        # arrange
        ingredients = create_complete_ingredients()
        ingredients.knowledge.database[2].biggest_id = 10

        # act
        actual_result = ingredients.deconstructor.model_fusion(
            ingredients.relative)

        # assert
        assert actual_result.identifier == 11
        assert actual_result.knowledge_level == 2
        assert actual_result.knowledge_domain == "C"
        assert actual_result.t_min == min([ingredients.pragmatic.min_timestamp,
                                           ingredients.relative.min_timestamp])
        assert actual_result.t_max == max([ingredients.pragmatic.max_timestamp,
                                           ingredients.relative.max_timestamp])
        assert actual_result.subject == ingredients.pragmatic.subject
        assert actual_result.aim == ingredients.pragmatic.aim

    def test_deconstruct_all_contraints_failed(self):
        # arrange
        ingredients = create_complete_ingredients()
        ingredients.deconstructor.feature_constraint = lambda *args: False
        ingredients.deconstructor.reliability_constraint = lambda *args: False
        log = self.DeconstructionInfo()
        ingredients.deconstructor.block = (
            ingredients.pragmatic.trained_with(ingredients.knowledge)
        )
        ingredients.deconstructor.relative_block = (
            ingredients.relative.trained_with(ingredients.knowledge)
        )

        # act
        actual_result = ingredients.deconstructor.deconstruct(
            ingredients.relative,
            log
        )

        # assert
        assert actual_result.inserted == [ingredients.pragmatic]
        assert actual_result.deleted == []


class TestZSigmaDeconstructor:
    from conML.shared.logger import DeconstructionInfo
    from conML.shared.logger import ReconstructionInfo

    @classmethod
    def create_complete_ingredients(cls):
        from collections import deque, namedtuple
        from conML.domain.deconstruction.z_sigma import ZSigmaDeconstructor

        knowledge = KnowledgeDatabase(8, build_block_from_rows)
        deconstructor = create_deconstructor()
        pragmatic, relative = build_pragmatic_and_relative()
        result_queue = deque()

        complete = ZSigmaDeconstructor(
            2,
            knowledge,
            deconstructor.state.settings,
            deconstructor.state.reconstructor,
            pragmatic,
            [relative],
            result_queue,
        )

        Ingredients = namedtuple("Ingredients",
                                 ("deconstructor, result_deque, "
                                  "pragmatic, relative, knowledge"))

        return Ingredients(complete, result_queue, pragmatic, relative,
                           knowledge)

    def test_time_contraint_max_distance_t_is_zero_success(self):
        # arrange
        ingredients = self.create_complete_ingredients()
        ingredients.deconstructor.settings.deconst_max_distance_t = 0.0
        ingredients.pragmatic.meta.t_max = int(
            datetime.datetime.strptime("2020", "%Y").timestamp()
        )
        ingredients.relative.meta.t_min = int(
            datetime.datetime.strptime("2021", "%Y").timestamp()
        )

        # assert
        actual_result = ingredients.deconstructor.time_constraint(
            ingredients.relative
        )

        # act
        assert actual_result is True

    def test_time_contraint_max_distance_t_is_zero_failure(self):
        # arrange
        ingredients = self.create_complete_ingredients()
        ingredients.pragmatic.meta.t_max = int(
            datetime.datetime.strptime("2021", "%Y").timestamp()
        )
        ingredients.pragmatic.meta.t_min = int(
            datetime.datetime.strptime("2020", "%Y").timestamp()
        )
        ingredients.relative.meta.t_min = int(
            datetime.datetime.strptime("2019", "%Y").timestamp()
        )
        ingredients.relative.meta.t_max = int(
            datetime.datetime.strptime("2021", "%Y").timestamp()
        )
        ingredients.deconstructor.settings.deconst_max_distance_t = 0.0

        # act
        actual_result = ingredients.deconstructor.time_constraint(
            ingredients.relative
        )

        # assert
        assert actual_result is False

    def test_time_contraint_max_distance_t_between_zero_and_one_success(self):
        # arrange
        ingredients = self.create_complete_ingredients()
        ingredients.deconstructor.settings.deconst_max_distance_t = 0.5
        ingredients.pragmatic.meta.t_max = int(
            datetime.datetime.strptime("2013", "%Y").timestamp()
        )
        ingredients.pragmatic.meta.t_min = int(
            datetime.datetime.strptime("2011", "%Y").timestamp()
        )
        ingredients.relative.meta.t_min = int(
            datetime.datetime.strptime("2012", "%Y").timestamp()
        )
        ingredients.relative.meta.t_max = int(
            datetime.datetime.strptime("2013", "%Y").timestamp()
        )

        actual_result = ingredients.deconstructor.time_constraint(
            ingredients.relative
        )

        # assert
        assert actual_result is True

    def test_time_contraint_max_distance_one(self):
        # arrange
        ingredients = self.create_complete_ingredients()
        ingredients.deconstructor.settings.deconst_max_distance_t = 1.0

        # act
        actual_result = ingredients.deconstructor.time_constraint(
            ingredients.relative
        )

        # assert
        assert actual_result is True

    def test_overlapping_time_success(self):
        # arrange
        ingredients = self.create_complete_ingredients()
        ingredients.deconstructor.settings.deconst_max_distanct_t = 0
        ingredients.pragmatic.meta.max_timestamp = int(
            datetime.datetime.strptime("2020", "%Y").timestamp()
        )
        ingredients.relative.meta.min = int(
            datetime.datetime.strptime("2021", "%Y").timestamp()
        )

        # assert
        actual_result = ingredients.deconstructor.overlapping_time(
            ingredients.pragmatic, ingredients.relative
        )

        # act
        assert actual_result is True

    def test_overlapping_time_returns_false(self):
        # arrange
        ingredients = self.create_complete_ingredients()
        ingredients.deconstructor.settings.deconst_max_distanct_t = 0
        ingredients.pragmatic.meta.t_max = int(
            datetime.datetime.strptime("2010", "%Y").timestamp()
        )
        ingredients.relative.meta.t_min = int(
            datetime.datetime.strptime("2008", "%Y").timestamp()
        )
        ingredients.pragmatic.meta.t_min = int(
            datetime.datetime.strptime("2007", "%Y").timestamp()
        )
        ingredients.relative.meta.t_max = int(
            datetime.datetime.strptime("2008", "%Y").timestamp()
        )

        # assert
        actual_result = ingredients.deconstructor.overlapping_time(
            ingredients.pragmatic, ingredients.relative
        )

        # act
        assert actual_result is False

    def test_enclosing_time_max_distance_t_between_zero_and_one_success(self):
        # arrange
        ingredients = self.create_complete_ingredients()
        ingredients.deconstructor.settings.deconst_max_distance_t = 0.5
        ingredients.pragmatic.meta.t_max = int(
            datetime.datetime.strptime("2013", "%Y").timestamp()
        )
        ingredients.pragmatic.meta.t_min = int(
            datetime.datetime.strptime("2011", "%Y").timestamp()
        )
        ingredients.relative.meta.t_min = int(
            datetime.datetime.strptime("2012", "%Y").timestamp()
        )
        ingredients.relative.meta.t_max = int(
            datetime.datetime.strptime("2013", "%Y").timestamp()
        )

        actual_result = ingredients.deconstructor.enclosing_time(
            ingredients.pragmatic, ingredients.relative
        )

        # assert
        assert actual_result is True

    def test_identify_overlapping_features(self):
        # arrange
        ingredients = self.create_complete_ingredients()
        pragmatic_block = ingredients.pragmatic.trained_with(
            ingredients.knowledge
        )
        del pragmatic_block.df["0.0.6"]
        del pragmatic_block.df["0.0.5"]
        del pragmatic_block.df["0.0.4"]
        del pragmatic_block.df["0.0.3"]

        relative_block = ingredients.relative.trained_with(
            ingredients.knowledge
        )
        del relative_block.df["0.0.6"]
        del relative_block.df["0.0.5"]
        del relative_block.df["0.0.4"]
        del relative_block.df["0.0.3"]

        # act
        ingredients.deconstructor.block = pragmatic_block
        ingredients.deconstructor.relative_block = relative_block

        actual_result = ingredients.deconstructor.identify_overlapping_features(
        )

        # assert
        assert actual_result.columns() == ["0.0.1", "0.0.2", "T", "Sigma", "Z"]
        assert actual_result.subject == pragmatic_block.subject
        assert actual_result.subject == relative_block.subject
        assert actual_result.n_rows() == (pragmatic_block.n_rows() +
                                          relative_block.n_rows())
        assert set(actual_result.origin) == set(
            pragmatic_block.origin + relative_block.origin
        )

    def test_column_constaint_success(self):
        # arrange
        ingredients = self.create_complete_ingredients()
        overlapped_block = ingredients.relative.trained_with(
            ingredients.knowledge
        )

        # act
        actual_result = ingredients.deconstructor.column_constraint(
            overlapped_block
        )

        # assert
        assert actual_result is True

    def test_column_constaint_failure(self):
        # arrange
        ingredients = self.create_complete_ingredients()
        overlapped_block = ingredients.relative.trained_with(
            ingredients.knowledge
        )
        del overlapped_block.df["0.0.1"]
        del overlapped_block.df["0.0.2"]
        del overlapped_block.df["0.0.3"]
        del overlapped_block.df["0.0.4"]
        del overlapped_block.df["0.0.5"]
        del overlapped_block.df["0.0.6"]

        # act
        actual_result = ingredients.deconstructor.column_constraint(
            overlapped_block
        )

        # assert
        assert actual_result is False

    def test_reconstructing_success(self):
        # arrange
        ingredients = self.create_complete_ingredients()
        ingredients.knowledge.database[2].biggest_id = 10
        log = self.DeconstructionInfo()
        overlapping = ingredients.relative.trained_with(
            ingredients.knowledge
        )
        relative = ingredients.relative
        ingredients.knowledge.insert(relative)

        meta = ingredients.deconstructor.model_fusion(
            ingredients.relative
        )

        # act
        actual_result = ingredients.deconstructor.reconstructing(
            overlapping, meta, relative, log
        )

        # assert
        winner = actual_result.inserted[0]
        assert winner.uid == "C.2.11"
        assert relative in actual_result.deleted
        assert winner in ingredients.knowledge
        assert relative not in ingredients.knowledge

    def test_reconstructing_failure(self):
        # arrange
        ingredients = self.create_complete_ingredients()
        ingredients.knowledge.database[2].biggest_id = 12
        ingredients.deconstructor.settings.deconst_strategy = "conservative"

        log = self.DeconstructionInfo()
        overlapping = ingredients.relative.trained_with(
            ingredients.knowledge
        )
        relative = ingredients.relative
        ingredients.knowledge.insert(relative)

        meta = ingredients.deconstructor.model_fusion(
            ingredients.relative
        )

        infos = [self.ReconstructionInfo(), self.ReconstructionInfo]
        infos[0].state = self.ReconstructionInfo.State.DISCARDED
        ingredients.deconstructor.reconstructor.reconstruct = (
            lambda *args, **kwargs: (None, [], infos))

        # act
        actual_result = ingredients.deconstructor.reconstructing(
            overlapping, meta, relative, log
        )

        # assert
        assert actual_result.inserted == []
        assert ingredients.pragmatic in actual_result.deleted

    def test_deconstruct_success(self):
        # arrange
        ingredients = self.create_complete_ingredients()
        ingredients.deconstructor.settings.deconst_max_distance_t = 1.0
        ingredients.knowledge.insert(ingredients.relative)
        ingredients.deconstructor.time_constraint = lambda x: True
        ingredients.deconstructor.column_constraint = lambda x: True
        ingredients.deconstructor.settings.force_time_expansion = False
        ingredients.deconstructor.block = \
            ingredients.pragmatic.trained_with(ingredients.knowledge)
        ingredients.deconstructor.relative_block = \
            ingredients.relative.trained_with(ingredients.knowledge)

        log = ingredients.deconstructor.log_info(
            ingredients.relative,
            ingredients.pragmatic
        )

        # act
        alog = ingredients.deconstructor.deconstruct(ingredients.relative, log)

        # assert
        assert alog.state == DeconstructionInfo.State.DECONSTRUCTED

    def test_deconstruct_failure(self):
        # arrange
        ingredients = self.create_complete_ingredients()
        ingredients.knowledge.insert(ingredients.relative)
        log = ingredients.deconstructor.log_info(
            ingredients.relative,
            ingredients.pragmatic
        )
        ingredients.deconstructor.time_constraint = lambda *a: False

        # act
        alog = ingredients.deconstructor.deconstruct(ingredients.relative, log)

        # assert
        assert alog.state == DeconstructionInfo.State.FAILED


class TestTSigmaDeconstructorSerial:

    @classmethod
    def create_complete_ingredients(cls):
        from collections import namedtuple
        from conML.ports.source_adapter import build_new_learnblock
        from conML.ports.krippendorff_adapter import krippendorff_alpha
        from conML.shared.settings import specific_settings_factory
        from conML.domain.deconstruction.t_sigma import TSigmaDeconstructor

        knowledge = KnowledgeDatabase(8, build_block_from_rows)
        pragmatic, relative = build_pragmatic_and_relative()

        tsigma = TSigmaDeconstructor(
            build_new_learnblock,
            krippendorff_alpha,
            knowledge,
            specific_settings_factory("deconstruction"),
            specific_settings_factory("general"),
        )

        Ingredients = namedtuple("Ingredients",
                                 ("deconstructor, "
                                  "pragmatic, relative, knowledge"))

        return Ingredients(tsigma, pragmatic, relative, knowledge)

    def test_level_constraint_success(self):
        # arrange
        ingredients = self.create_complete_ingredients()
        ingredients.deconstructor.general_settings.highest_level = 6

        # act
        actual_result = ingredients.deconstructor.level_constraint(7)

        # assert
        assert actual_result is False

    def test_level_constraint_failure(self):
        # arrange
        ingredients = self.create_complete_ingredients()
        ingredients.deconstructor.general_settings.highest_level = 6

        # act
        actual_result = ingredients.deconstructor.level_constraint(5)

        # assert
        assert actual_result is True

    def test_identify_overlapping_block(self):
        # arrange
        ingredients = self.create_complete_ingredients()

        df_1 = pd.DataFrame(
            {
                "0.0.1": [0.1, 0.2, 0.3, 0.4, 0.5],
                "T": [datetime.datetime.strptime(
                            "2020-01-01 08:00", "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime(
                            "2020-01-01 08:01", "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime(
                            "2020-01-01 08:02", "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime(
                            "2020-01-01 08:06", "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime(
                            "2020-01-01 08:07", "%Y-%m-%d %H:%M")],
                "Sigma": [" " for _ in range(5)],
                "Z": [2, 2, 1, 2, 2]
            }
        )
        block_pragmatic = get_labeled_block(df_1)

        df_2 = pd.DataFrame(
            {
                "0.0.1": [0.1, 0.2, 0.3, 0.4, 0.5],
                "T": [datetime.datetime.strptime(
                            "2020-01-01 08:00", "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime(
                            "2020-01-01 08:01", "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime(
                            "2020-01-01 08:02", "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime(
                            "2020-01-01 08:03", "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime(
                            "2020-01-01 08:04", "%Y-%m-%d %H:%M")],
                "Sigma": [" " for _ in range(5)],
                "Z": [2, 2, 1, 2, 1]
            }
        )
        block_relative = get_labeled_block(df_2)

        # act
        ingredients.relative.trained_with = lambda *args: block_relative
        ingredients.deconstructor.block = \
            ingredients.pragmatic.trained_with(ingredients.knowledge)
        ingredients.deconstructor.relative_block = \
            ingredients.relative.trained_with(ingredients.knowledge)

        actual_result = ingredients.deconstructor.identify_overlapping_block(
            block_pragmatic
        )

        # assert
        assert actual_result.n_rows() == 3

    def test_row_and_reliability_constraint_success(self):
        # arrange
        ingredients = self.create_complete_ingredients()
        df_1 = pd.DataFrame(
            {
                "0.0.1": [0.1, 0.2, 0.3, 0.4, 0.5],
                "T": [
                    int(datetime.datetime.strptime("2020", "%Y").timestamp()),
                    int(datetime.datetime.strptime("2020", "%Y").timestamp()),
                    int(datetime.datetime.strptime("2020", "%Y").timestamp()),
                    int(datetime.datetime.strptime("2020", "%Y").timestamp()),
                    int(datetime.datetime.strptime("2020", "%Y").timestamp())],
                "Z_other": [2, 2, 1, 2, 2],
                "Z": [2, 2, 1, 2, 2]
            }
        )
        block_pragmatic = get_labeled_block(df_1)
        ingredients.deconstructor.settings.learn_block_minimum = 5
        ingredients.deconstructor.krippendorf = lambda x: -1.0

        # act
        actual_result = (
            ingredients.deconstructor.row_and_reliability_contraints(
                block_pragmatic)
        )

        # assert
        assert actual_result is True

    def test_row_constraint_failure(self):
        # arrange
        ingredients = self.create_complete_ingredients()
        df_1 = pd.DataFrame(
            {
                "0.0.1": [0.1, 0.2, 0.3, 0.4, 0.5],
                "T": [
                    int(datetime.datetime.strptime("2020", "%Y").timestamp()),
                    int(datetime.datetime.strptime("2020", "%Y").timestamp()),
                    int(datetime.datetime.strptime("2020", "%Y").timestamp()),
                    int(datetime.datetime.strptime("2020", "%Y").timestamp()),
                    int(datetime.datetime.strptime("2020", "%Y").timestamp())],
                "Z_other": [2, 2, 1, 2, 2],
                "Z": [2, 2, 1, 2, 2]
            }
        )
        block_pragmatic = get_labeled_block(df_1)
        ingredients.deconstructor.settings.learn_block_minimum = 6

        # act
        actual_result = (
            ingredients.deconstructor.row_constraint(block_pragmatic)
        )

        # assert
        assert actual_result is False

    def test_reliability_constraint_success(self):
        # arrange
        ingredients = self.create_complete_ingredients()
        ingredients.deconstructor.settings.min_reliability = 0.6
        ingredients.deconstructor.settings.allow_weak_reliability = True
        df_1 = pd.DataFrame(
            {
                "0.0.1": [0.1, 0.2, 0.3, 0.4, 0.5],
                "T": [
                    int(datetime.datetime.strptime("2020", "%Y").timestamp()),
                    int(datetime.datetime.strptime("2020", "%Y").timestamp()),
                    int(datetime.datetime.strptime("2020", "%Y").timestamp()),
                    int(datetime.datetime.strptime("2020", "%Y").timestamp()),
                    int(datetime.datetime.strptime("2020", "%Y").timestamp())],
                "Z_other": [2, 2, 1, 2, 2],
                "Z": [2, 2, 1, 2, 2]
            }
        )
        block_pragmatic = get_labeled_block(df_1)
        ingredients.deconstructor.krippendorf = lambda x: 0.5

        # act
        actual_result = (
            ingredients.deconstructor.reliability_constraint(block_pragmatic)
        )

        # assert
        assert actual_result is True

    def test_reliability_constraint_failure(self):
        # arrange
        ingredients = self.create_complete_ingredients()
        df_1 = pd.DataFrame(
            {
                "0.0.1": [0.1, 0.2, 0.3, 0.4, 0.5],
                "T": [
                    int(datetime.datetime.strptime("2020", "%Y").timestamp()),
                    int(datetime.datetime.strptime("2020", "%Y").timestamp()),
                    int(datetime.datetime.strptime("2020", "%Y").timestamp()),
                    int(datetime.datetime.strptime("2020", "%Y").timestamp()),
                    int(datetime.datetime.strptime("2020", "%Y").timestamp())],
                "Z_other": [2, 2, 1, 2, 0],
                "Z": [2, 2, 1, 2, 2]
            }
        )
        block_pragmatic = get_labeled_block(df_1)
        ingredients.deconstructor.settings.min_reliability = 0.5
        ingredients.deconstructor.settings.allow_weak_reliability = False

        # act
        actual_result = (
            ingredients.deconstructor.reliability_constraint(block_pragmatic)
        )

        # assert
        assert actual_result is not True

    def test_prepare_higher_block_success(self):
        # arrange
        ingredients = self.create_complete_ingredients()
        df_1 = pd.DataFrame(
            {
                "0.0.1": [0.1, 0.2, 0.3, 0.4, 0.5],
                "T": [
                    int(datetime.datetime.strptime("2020", "%Y").timestamp()),
                    int(datetime.datetime.strptime("2020", "%Y").timestamp()),
                    int(datetime.datetime.strptime("2020", "%Y").timestamp()),
                    int(datetime.datetime.strptime("2020", "%Y").timestamp()),
                    int(datetime.datetime.strptime("2020", "%Y").timestamp())],
                "Z_other": [2, 2, 1, 2, 0],
                "Z": [2, 2, 1, 2, 2]
            }
        )
        overlapping = get_labeled_block(df_1)
        ingredients.pragmatic.identifier = 20
        ingredients.relative.identifier = 21

        # act
        actual_result = ingredients.deconstructor.prepare_higher_block(
            ingredients.pragmatic, ingredients.relative, overlapping
        )

        # assert
        assert "C.2.21" in actual_result.columns(effective=True)
        assert "C.2.20" in actual_result.columns(effective=True)
        assert "Z" in actual_result.columns()
        assert "Sigma" in actual_result.columns()
        assert "T" in actual_result.columns()
        assert actual_result.origin == ("C.2.20", "C.2.21")
        assert actual_result.n_rows() == 5
        assert actual_result.n_columns() == 5
        assert actual_result.subject is None

    def test_deconstruct_success(self):
        # arrange
        ingredients = self.create_complete_ingredients()
        log = DeconstructionInfo()

        df_1 = pd.DataFrame(
            {
                "0.0.1": [0.1, 0.2, 0.3, 0.4, 0.5],
                "0.0.2": [0.1, 0.2, 0.3, 0.4, 0.5],
                "T": [
                    datetime.datetime.strptime(
                        "2020-01-01 08:00", "%Y-%m-%d %H:%M"),
                    datetime.datetime.strptime(
                        "2020-01-01 08:01", "%Y-%m-%d %H:%M"),
                    datetime.datetime.strptime(
                        "2020-01-01 08:02", "%Y-%m-%d %H:%M"),
                    datetime.datetime.strptime(
                        "2020-01-01 08:03", "%Y-%m-%d %H:%M"),
                    datetime.datetime.strptime(
                        "2020-01-01 08:04", "%Y-%m-%d %H:%M")],
                "Sigma": ["Kme" for _ in range(5)],
                "Z": [2, 2, 1, 2, 2]
            }
        )
        block_pragmatic = get_labeled_block(df_1)
        ingredients.pragmatic.trained_with = lambda *args: block_pragmatic

        df_2 = pd.DataFrame(
            {
                "0.0.2": [0.1, 0.2, 0.3, 0.4, 0.5],
                "0.0.3": [0.1, 0.2, 0.3, 0.4, 0.5],
                "T": [
                    datetime.datetime.strptime(
                        "2020-01-01 08:00", "%Y-%m-%d %H:%M"),
                    datetime.datetime.strptime(
                        "2020-01-01 08:01", "%Y-%m-%d %H:%M"),
                    datetime.datetime.strptime(
                        "2020-01-01 08:02", "%Y-%m-%d %H:%M"),
                    datetime.datetime.strptime(
                        "2020-01-01 08:03", "%Y-%m-%d %H:%M"),
                    datetime.datetime.strptime(
                        "2020-01-01 08:07", "%Y-%m-%d %H:%M")],
                "Sigma": ["Kme" for _ in range(5)],
                "Z": [2, 2, 1, 2, 1]
            }
        )
        block_relative = get_labeled_block(df_2)
        ingredients.relative.trained_with = lambda *args: block_relative
        ingredients.deconstructor.row_and_reliability_contraints = (
            lambda *args: True
        )
        ingredients.deconstructor.block = block_pragmatic
        ingredients.deconstructor.relative_block = block_relative

        # act
        ingredients.deconstructor.deconstruct(
            2,
            ingredients.relative,
            ingredients.pragmatic,
            log
        )

        # assert
        assert len(ingredients.deconstructor.temp_changes[0]) == 3

    def test_deconstruct_failure(self):
        # arrange
        ingredients = self.create_complete_ingredients()
        log = DeconstructionInfo()

        # act
        ingredients.deconstructor.relative_block = ingredients.relative.trained_with(
            ingredients.knowledge
        )
        ingredients.deconstructor.block = ingredients.pragmatic.trained_with(
            ingredients.knowledge
        )

        ingredients.deconstructor.deconstruct(
            2,
            ingredients.relative,
            ingredients.pragmatic,
            log
        )

        # assert
        assert len(ingredients.deconstructor.temp_changes) == 0

    def test_run_serial_success(self):
        from collections import deque

        # arrange
        ingredients = self.create_complete_ingredients()
        df_1 = pd.DataFrame(
            {
                "0.0.1": [0.1, 0.2, 0.3, 0.4, 0.5],
                "0.0.2": [0.1, 0.2, 0.3, 0.4, 0.5],
                "T": [datetime.datetime.strptime("2020-01-01 08:00", "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime("2020-01-01 08:01", "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime("2020-01-01 08:02", "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime("2020-01-01 08:03", "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime("2020-01-01 08:04", "%Y-%m-%d %H:%M")],
                "Sigma": ["Kme" for _ in range(5)],
                "Z": [2, 2, 1, 2, 2]
            }
        )
        block_pragmatic = get_labeled_block(df_1)
        ingredients.pragmatic.trained_with = lambda *args: block_pragmatic
        ingredients.pragmatic.identifier = 11

        df_2 = pd.DataFrame(
            {
                "0.0.2": [0.1, 0.2, 0.3, 0.4, 0.5],
                "0.0.3": [0.1, 0.2, 0.3, 0.4, 0.5],
                "T": [datetime.datetime.strptime("2020-01-01 08:00", "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime("2020-01-01 08:01", "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime("2020-01-01 08:02", "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime("2020-01-01 08:03", "%Y-%m-%d %H:%M"),
                      datetime.datetime.strptime("2020-01-01 08:07", "%Y-%m-%d %H:%M")],
                "Sigma": ["Kme" for _ in range(5)],
                "Z": [2, 2, 1, 2, 1]
            }
        )
        block_relative = get_labeled_block(df_2)
        ingredients.relative.trained_with = (lambda *args: block_relative)
        ingredients.relative.identifer = 21
        ingredients.deconstructor.row_and_reliability_contraints = (
            lambda *args: True
        )

        # act
        result_deque = deque()
        ingredients.deconstructor.run_serial(
            2,
            [ingredients.relative],
            ingredients.pragmatic,
            result_deque
        )

        # assert
        log, next_level, high_block = result_deque.pop()
        assert log.pragmatic == ingredients.pragmatic
        assert log.relative == ingredients.relative
        assert log.new_learnblock == high_block
        assert next_level == 3
        assert log.state == DeconstructionInfo.State.DECONSTRUCTED
        assert log.relationship == ("Sigma", "T")


class TestAbstract:

    def test_model_disposal(self):
        # arrange
        knowledge = KnowledgeDatabase(8, build_block_from_rows)
        deconstructor = create_deconstructor()
        pragmatic, relative = build_pragmatic_and_relative()

        result_queue = deque()
        abstract = Abstract(
            2,
            knowledge,
            deconstructor.state.settings,
            deconstructor.state.reconstructor,
            pragmatic,
            [relative],
            result_queue,
        )

        # act/ assert
        knowledge.insert(relative)
        block = pragmatic.trained_with(knowledge)
        relative_block = relative.trained_with(knowledge)
        deconstructor.state.settings.deconst_strategy = "conservative"
        abstract.block = block
        abstract.relative_block = relative_block
        disposal = abstract.model_disposal(pragmatic, relative)
        assert disposal.deleted == [pragmatic]
        assert disposal.inserted == []

        knowledge.insert(pragmatic)
        deconstructor.state.settings.deconst_strategy = "integrative"
        disposal = abstract.model_disposal(pragmatic, relative)
        assert disposal.deleted == []
        assert disposal.inserted == [pragmatic]

        knowledge.insert(pragmatic)
        deconstructor.state.settings.deconst_strategy = "oppurtunistic"
        disposal = abstract.model_disposal(pragmatic, relative)
        relative_block = relative.trained_with(knowledge)
        relative_block.n_rows = lambda: 5
        assert disposal.deleted == [relative]
        assert disposal.inserted == [pragmatic]

        disposal = abstract.model_disposal(pragmatic, relative)
        relative_block = relative.trained_with(knowledge)
        relative_block.n_rows = lambda: 200
        assert list(disposal.deleted) == [pragmatic]
        assert list(disposal.inserted) == [relative]

    def test_not_impletend_deconstruct(self):
        # arrange
        knowledge = KnowledgeDatabase(8, build_block_from_rows)
        deconstructor = create_deconstructor()
        pragmatic, relative = build_pragmatic_and_relative()

        result_queue = deque()
        abstract = Abstract(
            2,
            knowledge,
            deconstructor.state.settings,
            deconstructor.state.reconstructor,
            pragmatic,
            [relative],
            result_queue,
        )

        # act/assert
        with pytest.raises(NotImplementedError):
            abstract.deconstruct()

    @patch("conML.domain.deconstruction.abstract.Abstract.deconstruct",
           lambda *args: None)
    @patch("conML.domain.deconstruction.abstract.Abstract.log_info")
    def test_run(self, mock_log_info):
        # arrange
        knowledge = KnowledgeDatabase(8, build_block_from_rows)
        deconstructor = create_deconstructor()
        deconstructor.state.settings.deconst_mode = "minimal"

        pragmatic, relative = build_pragmatic_and_relative()
        log = DeconstructionInfo(
            relative=relative, pragmatic=pragmatic, relationship=None)
        log.state = DeconstructionInfo.State.DECONSTRUCTED
        mock_log_info.return_value = log

        result_queue = deque()
        abstract = Abstract(
            2,
            knowledge,
            deconstructor.state.settings,
            deconstructor.state.reconstructor,
            pragmatic,
            [relative],
            result_queue,
        )

        # act
        abstract.run()

        # assert
        for log in result_queue:
            assert log.state == DeconstructionInfo.State.DECONSTRUCTED

    def test_log_info_relationship_is_none(self):
        # arrange
        knowledge = KnowledgeDatabase(8, build_block_from_rows)
        deconstructor = create_deconstructor()
        pragmatic, relative = build_pragmatic_and_relative()

        result_queue = deque()
        abstract = Abstract(
            2,
            knowledge,
            deconstructor.state.settings,
            deconstructor.state.reconstructor,
            pragmatic,
            [relative],
            result_queue,
        )

        # act
        log = abstract.log_info(relative, pragmatic)

        # assert
        assert log.relationship is None


class TestAbstractDeconstructor:
    def test_find_relatives_str(self):
        # arrange
        deconstructor = create_deconstructor()

        # assert
        assert str(deconstructor) == "\n".join([
            str(deconstructor.state.settings),
            str(deconstructor.state.reconstructor)
        ])

    def test_find_relatives_repr(self):
        # arrange
        deconstructor = create_deconstructor()

        # assert
        assert repr(deconstructor) == ", ".join([
            "{}".format(repr(deconstructor.state.settings)),
            "{}".format(repr(deconstructor.state.reconstructor))
        ])

    def test_to_dict(self):
        # arrange
        deconstructor = create_deconstructor()

        # act
        d = deconstructor.state.to_dict()

        # assert
        assert d == {
            "general_settings": deconstructor.state.general_settings,
            "block_processing_settings": deconstructor.state.block_processing_settings,
            "deconstruction_settings": deconstructor.state.settings,
            "reconstructor": deconstructor.state.reconstructor,
            "learnblock_constructor": deconstructor.state.learnblock_constructor,
            "cluster_finder": deconstructor.state.cluster_finder
        }

    def test_find_relatives_success(self):
        # arrange
        deconstructor = create_deconstructor()
        knowledge = KnowledgeDatabase(8, build_block_from_rows)
        knowledge.observer.append(RelativeFinder(8))
        pragmatic, _ = build_pragmatic_and_relative()

        # act
        it = deconstructor.state.find_relatives(knowledge, pragmatic)

        # assert
        assert next(it) == []
        assert next(it) == []
        assert next(it) == []

    def test_clean_ts_queue(self):
        # arrange
        pragmatic, relative = build_pragmatic_and_relative()
        deconstructor = create_deconstructor()
        knowledge = KnowledgeDatabase(8, build_block_from_rows)
        lb = pragmatic.trained_with(knowledge)
        deconstructor.tsigma_beamer.append((pragmatic, lb))
        relative.meta.knowledge_level = 1
        relative.meta.knowledge_domain = "C"
        relative.meta.identifier = 1

        # act
        deconstructor.state.clean_ts_queue([relative])

        # assert
        assert len(deconstructor.tsigma_beamer) == 0


class TestSerielDeconstructor:
    def test_str(self):
        # arrange
        deconstructor = create_deconstructor()

        # assert
        assert str(deconstructor.state) == "\n".join([
            str(deconstructor.state.settings),
            str(deconstructor.state.reconstructor)
        ])

    def test_repr(self):
        # arrange
        deconstructor = create_deconstructor()

        # assert
        assert repr(deconstructor.state) == ", ".join([
            "{}".format(repr(deconstructor.state.settings)),
            "{}".format(repr(deconstructor.state.reconstructor))
        ])

    def test_deconstruct_success(self):
        # arrange
        deconstructor = create_deconstructor()
        knowledge_database = KnowledgeDatabase(7, build_block_from_rows)
        pragmatic, relative = build_pragmatic_and_relative()
        knowledge_database.observer.append(RelativeFinder(7))
        knowledge_database.insert(relative)

        def find(*args, **kwargs):
            yield [relative]
            yield [relative]
            yield [relative]

        deconstructor.state.find_relatives = find

        it = iter(deconstructor.state.deconstruct(2, pragmatic, knowledge_database))
        complete = next(it)
        assert next(iter(complete)).state == DeconstructionInfo.State.DECONSTRUCTED

        t_sigma = next(it)
        knowledge_database.insert(relative)

        z_sigma = next(it)
        knowledge_database.insert(relative)
        assert next(iter(z_sigma)).state == DeconstructionInfo.State.FAILED

    @patch("conML.domain.deconstruction.t_sigma.TSigmaDeconstructor.run_serial")
    def test_t_sigma_deconstruction(self, mock_run_serial):
        # arrange
        level = 1
        deconstructor = create_deconstructor()
        knowledge_database = KnowledgeDatabase(7, build_block_from_rows)
        pragmatic, relative = build_pragmatic_and_relative()

        def run(level, relatives, pragmatic, result_deque):
            result_deque.append((DeconstructionInfo(inserted=[pragmatic],
                                                    deleted=[]),
                                1,
                                pragmatic.trained_with(knowledge_database))
            )

        mock_run_serial.side_effect = run

        # act
        for log in deconstructor.state.run_t_sigma_deconstruction(
                    level, pragmatic, knowledge_database, [relative]):

            # assert
            assert log.inserted == [pragmatic]
            assert log.deleted == []


class TestParallelDeconstructor:

    def test_str(self):
        # arrange
        deconstructor = create_deconstructor()
        deconstructor.transit()

        # assert
        assert str(deconstructor.state) == "\n".join([
            str(deconstructor.state.settings),
            str(deconstructor.state.reconstructor)
        ])

    def test_repr(self):
        # arrange
        deconstructor = create_deconstructor()
        deconstructor.transit()

        # assert
        assert repr(deconstructor.state) == ", ".join([
            "{}".format(repr(deconstructor.state.settings)),
            "{}".format(repr(deconstructor.state.reconstructor))
        ])

    @patch("conML.domain.deconstruction.deconstruction.CompleteDeconstructor")
    def test_run_complete_deconstruction(self, complete):
        # arrange
        level = 1
        deconstructor = create_deconstructor()
        knowledge_database = KnowledgeDatabase(7, build_block_from_rows)
        pragmatic, relative = build_pragmatic_and_relative()

        class D:
            def __init__(s, level, knowledge, settings, reconstructor,
                         pragmatic, relatives, result_queue, cluster_finder,
                         general_settings, block_processing_settings):
                s.level = level
                s.knowledge = knowledge
                s.settings = settings
                s.reconstructor = reconstructor
                s.pragmatic = pragmatic
                s.relatives = relatives
                s.result_queue = result_queue
                s.block = pragmatic.trained_with(knowledge)
                s.cluster_finder = cluster_finder
                s.general_settings = general_settings
                s.block_processing_settings = block_processing_settings

            def run(s):
                s.result_queue.append(
                    DeconstructionInfo(inserted=[pragmatic], deleted=[])
                )

        complete.side_effect = D

        # act
        deconstructor.transit()
        for log in deconstructor.state.run_complete_deconstruction(
                level, pragmatic, knowledge_database, [relative]):
            # assert
            assert log.deleted == []
            assert log.inserted == [pragmatic]

    def test_run_t_sigma_deconstruction(self):
        from multiprocessing import Manager

        # arrange
        level = 1
        deconstructor = create_deconstructor()
        deconstructor.transit()
        knowledge_database = KnowledgeDatabase(7, build_block_from_rows)
        pragmatic, relative = build_pragmatic_and_relative()

        # act
        put_mock = MagicMock()
        put_mock.put = lambda x: True
        deconstructor.state.tsigma_inputs = put_mock

        wait_mock = MagicMock()
        wait_mock.wait = lambda: True
        deconstructor.state.apply_event = wait_mock

        lb = create_pmlm().trained_with(knowledge_database)

        q = Manager().Queue()
        q.put(
            (DeconstructionInfo(state=DeconstructionInfo.State.DECONSTRUCTED),
            1, lb))
        q.put(
            (DeconstructionInfo(state=DeconstructionInfo.State.DECONSTRUCTED),
             1, lb)
        )

        deconstructor.state.tsigma_outputs = q

        # assert
        for log in deconstructor.state.run_t_sigma_deconstruction(
            knowledge_database, pragmatic
        ):
            assert log.inserted == [pragmatic]
            assert log.deleted == []
            assert deconstructor.state.tsigma_beamer

    @patch("conML.domain.deconstruction.deconstruction.ZSigmaDeconstructor")
    def test_sigma_z_deconstruction(self, z_sigma):
        # arrange
        level = 1
        deconstructor = create_deconstructor()
        knowledge_database = KnowledgeDatabase(7, build_block_from_rows)
        pragmatic, relative = build_pragmatic_and_relative()

        class D:
            def __init__(s, level, knowledge, settings, reconstructor,
                         pragmatic, relatives, result_queue):
                s.level = level
                s.knowledge = knowledge
                s.settings = settings
                s.reconstructor = reconstructor
                s.pragmatic = pragmatic
                s.relatives = relatives
                s.result_queue = result_queue
                s.block = pragmatic.trained_with(knowledge)

            def run(s):
                s.result_queue.append(
                    DeconstructionInfo(inserted=[pragmatic], deleted=[])
                )

        z_sigma.side_effect = D

        # act
        deconstructor.transit()
        for log in deconstructor.state.run_sigma_z_deconstruction(
                level, pragmatic, knowledge_database, [relative]):
            # assert
            assert log.deleted == []
            assert log.inserted == [pragmatic]

    def test_start_background_process(self):
        # arrange
        deconstructor = create_deconstructor()
        knowledge_database = KnowledgeDatabase(7, build_block_from_rows)
        deconstructor.transit()

        # act
        deconstructor.state.start_background_tsigma(knowledge_database)

        # assert
        assert deconstructor.state.tsigma_process
        assert deconstructor.state.tsigma_process.daemon is True

    def test_deconstruct(self):
        # arrange
        deconstructor = create_deconstructor()
        deconstructor.transit()
        knowledge_database = KnowledgeDatabase(7, build_block_from_rows)
        pragmatic, relative = build_pragmatic_and_relative()
        knowledge_database.observer.append(RelativeFinder(7))
        knowledge_database.insert(relative)

        def find(*args, **kwargs):
            yield [relative]
            yield [relative]
            yield [relative]

        deconstructor.state.find_relatives = find

        it = iter(deconstructor.state.deconstruct(
            2, pragmatic, knowledge_database)
        )

        complete = next(it)
        assert next(iter(complete)).state == DeconstructionInfo.State.DECONSTRUCTED

        t_sigma = next(it)
        knowledge_database.insert(relative)

        z_sigma = next(it)
        knowledge_database.insert(relative)
        assert next(iter(z_sigma)).state == DeconstructionInfo.State.FAILED


class TestDeconstructor:
    def test_transit(self):
        from conML.domain.deconstruction.deconstruction import SerialDeconstructor
        from conML.domain.deconstruction.deconstruction import ParallelDeconstructor

        # arrange
        deconstructor = create_deconstructor()

        # act/assert
        deconstructor.transit()
        assert isinstance(deconstructor.state, ParallelDeconstructor)

        deconstructor.transit()
        assert isinstance(deconstructor.state, SerialDeconstructor)

    def test_deconstruct_run_full(self):
        # arrange
        deconstructor = create_deconstructor()
        deconstructor.state.settings.deconst_model = "full"
        level = 1
        pragmatic, relative = build_pragmatic_and_relative()
        database = KnowledgeDatabase(7,  build_block_from_rows)

        def decon(*args, **kwargs):
            def complete():
                yield DeconstructionInfo(
                    state=DeconstructionInfo.State.FAILED
                )

            def tsigma():
                yield DeconstructionInfo(
                    state=DeconstructionInfo.State.FAILED
                )

            def zsigma():
                yield DeconstructionInfo(
                    state=DeconstructionInfo.State.FAILED
                )

            yield complete()
            yield tsigma()
            yield zsigma()

        deconstructor.state.deconstruct = decon

        # act
        for log in deconstructor.deconstruct(level, pragmatic, database):
            print(log)

    def test_deconstruct_run_minimal(self):
        # arrange
        deconstructor = create_deconstructor()
        deconstructor.state.settings.deconst_model = "minimal"
        level = 1
        pragmatic, relative = build_pragmatic_and_relative()
        database = KnowledgeDatabase(7,  build_block_from_rows)

        def decon(*args, **kwargs):
            def complete():
                yield DeconstructionInfo(
                    state=DeconstructionInfo.State.FAILED
                )

            def tsigma():
                yield DeconstructionInfo(
                    state=DeconstructionInfo.State.DECONSTRUCTED
                )

            def zsigma():
                yield DeconstructionInfo(
                    state=DeconstructionInfo.State.FAILED
                )

            yield complete()
            yield tsigma()
            yield zsigma()

        deconstructor.state.deconstruct = decon

        # act
        for log in deconstructor.deconstruct(level, pragmatic, database):
            print(log)

    def test_deconstruct_run_deleted_pragmatic(self):
        # arrange
        deconstructor = create_deconstructor()
        deconstructor.state.settings.deconst_model = "minimal"
        level = 1
        pragmatic, relative = build_pragmatic_and_relative()
        database = KnowledgeDatabase(7, build_block_from_rows)

        def decon(*args, **kwargs):
            def complete():
                yield DeconstructionInfo(
                    state=DeconstructionInfo.State.FAILED
                )

            def tsigma():
                yield DeconstructionInfo(
                    state=DeconstructionInfo.State.DECONSTRUCTED,
                    deleted=[pragmatic]
                )

            def zsigma():
                yield DeconstructionInfo(
                    state=DeconstructionInfo.State.FAILED
                )

            yield complete()
            yield tsigma()
            yield zsigma()

        deconstructor.state.deconstruct = decon

        # act
        it = iter(deconstructor.deconstruct(level, pragmatic, database))
        assert next(it).state == DeconstructionInfo.State.FAILED
        assert next(it).state == DeconstructionInfo.State.DECONSTRUCTED

    def test_deconstruct_close_t_sigma_process_after_deconstruction(self):
        # arrange
        deconstructor = create_deconstructor()
        deconstructor.state.settings.deconst_model = "minimal"
        level = 1
        pragmatic, relative = build_pragmatic_and_relative()
        database = KnowledgeDatabase(7, build_block_from_rows)
        database.observer.append(RelativeFinder(7))

        def decon(*args, **kwargs):
            def complete():
                yield DeconstructionInfo(
                    state=DeconstructionInfo.State.FAILED
                )

            def tsigma():
                yield DeconstructionInfo(
                    state=DeconstructionInfo.State.FAILED,
                )

            def zsigma():
                yield DeconstructionInfo(
                    state=DeconstructionInfo.State.FAILED
                )

            yield complete()
            yield tsigma()
            yield zsigma()

        deconstructor.transit()
        deconstructor.state.deconstruct = decon

        deconstructor.state.tsigma_process = MagicMock()
        deconstructor.state.tsigma_inputs = MagicMock()
        deconstructor.state.clear_event = MagicMock()
        deconstructor.state.tsigma_process = MagicMock()

        # act
        for _ in deconstructor.deconstruct(level, pragmatic, database):
            pass

        assert deconstructor.state.tsigma_inputs.put.called
        assert deconstructor.state.clear_event.wait.called

    def test_properties(self):
        # arrange
        deconstructor = create_deconstructor()

        # act/assert
        deconstructor.deconst_mode = "minimal"
        assert deconstructor.deconst_mode == "minimal"

        deconstructor.deconst_strategy = "conservative"
        assert deconstructor.deconst_strategy == "conservative"

        deconstructor.deconst_max_distance_t = 0.1
        assert deconstructor.deconst_max_distance_t == 0.1

        deconstructor.deconst_full_tolerance = 0.0
        assert deconstructor.deconst_full_tolerance == 0.0

        deconstructor.force_time_expansion = False
        assert deconstructor.force_time_expansion is False

        deconstructor.allow_weak_reliability = False
        assert deconstructor.allow_weak_reliability is False

        deconstructor.learn_block_minimum = 100
        assert deconstructor.learn_block_minimum == 100

        deconstructor.min_reliability = 0.5
        assert deconstructor.min_reliability == 0.5


class TestTSigmaDeconstructorParallel:

    @classmethod
    def create_tsigma_ingredients(cls):
        from collections import namedtuple
        from conML.ports.source_adapter import build_new_learnblock
        from conML.ports.krippendorff_adapter import krippendorff_alpha
        from conML.shared.settings import specific_settings_factory
        from conML.domain.deconstruction.t_sigma import TSigmaDeconstructor

        knowledge = KnowledgeDatabase(8, build_block_from_rows)
        pragmatic, relative = build_pragmatic_and_relative()

        tsigma = TSigmaDeconstructor(
            build_new_learnblock,
            krippendorff_alpha,
            knowledge,
            specific_settings_factory("deconstruction"),
            specific_settings_factory("general"),
        )

        Ingredients = namedtuple("Ingredients",
                                 ("deconstructor, "
                                  "pragmatic, relative, knowledge"))

        return Ingredients(tsigma, pragmatic, relative, knowledge)

    def test_upgrade(self):
        # arrange
        knowledge = KnowledgeDatabase(7, build_pragmatic_and_relative)
        lb = create_pmlm().trained_with(knowledge)
        ingredients = TestTSigmaDeconstructorParallel.create_tsigma_ingredients()
        ingredients.deconstructor.temp_changes.append(
            (DeconstructionInfo(), 2, lb)
        )

        # act
        ingredients.deconstructor.upgrade(["C.1.1"])

        # assert
        assert ingredients.deconstructor.temp_changes == []

    def test_apply(self):
        from multiprocessing import Manager
        knowledge = KnowledgeDatabase(7, build_pragmatic_and_relative)
        lb = create_pmlm().trained_with(knowledge)
        ingredients = TestTSigmaDeconstructorParallel.create_tsigma_ingredients()
        log = DeconstructionInfo(DeconstructionInfo.State.DECONSTRUCTED)
        level = 2
        ingredients.deconstructor.temp_changes.append((log, level, lb))
        ingredients.deconstructor.outputs = Manager().Queue()

        # act
        ingredients.deconstructor.apply()

        # assert
        a_log, a_level, a_lb = ingredients.deconstructor.outputs.get(False)
        assert log.state == DeconstructionInfo.State.DECONSTRUCTED
        assert a_level == level
        assert a_lb.origin == lb.origin

    def test_clean(self):
        # arrange
        from multiprocessing import Manager
        knowledge = KnowledgeDatabase(7, build_pragmatic_and_relative)
        ingredients = TestTSigmaDeconstructorParallel.create_tsigma_ingredients()
        ingredients.deconstructor.outputs = Manager().Queue()
        ingredients.deconstructor.outputs.put((
            None, None
        ))
        ingredients.deconstructor.inputs = Manager().Queue()
        ingredients.deconstructor.inputs.put((
            None, None
        ))

        # act
        ingredients.deconstructor.clean()

        # assert
        assert ingredients.deconstructor.inputs.empty()
        assert ingredients.deconstructor.outputs.empty()

    def test_run(self):
        from conML.domain.deconstruction.deconstruction import Start
        from conML.domain.deconstruction.deconstruction import Upgrade
        from conML.domain.deconstruction.deconstruction import Apply
        from conML.domain.deconstruction.deconstruction import Clean
        from conML.domain.deconstruction.deconstruction import Exit

        # arrange
        from multiprocessing import Manager
        pragmatic, relative = build_pragmatic_and_relative()

        ingredients = TestTSigmaDeconstructorParallel.create_tsigma_ingredients()
        ingredients.deconstructor.outputs = Manager().Queue()
        ingredients.deconstructor.inputs = Manager().Queue()
        ingredients.deconstructor.upgrade_event = MagicMock()
        ingredients.deconstructor.apply_event = MagicMock()
        ingredients.deconstructor.clear_event = MagicMock()
        ingredients.deconstructor.clean = lambda *args: None
        ingredients.deconstructor.deconstruct = lambda *args: None

        # act
        ingredients.deconstructor.inputs.put(Start(
            pragmatic=pragmatic,
            relatives=[relative],
            level=1
        ))
        ingredients.deconstructor.inputs.put(Upgrade(
            deletes=[pragmatic]
        ))
        ingredients.deconstructor.inputs.put(Apply())
        ingredients.deconstructor.inputs.put(Clean())
        ingredients.deconstructor.inputs.put(Exit())

        try:
            ingredients.deconstructor.run()

        except SystemExit:
            # assert
            assert ingredients.deconstructor.upgrade_event.set.called
            assert ingredients.deconstructor.apply_event.set.called
            assert ingredients.deconstructor.clear_event.set.called
