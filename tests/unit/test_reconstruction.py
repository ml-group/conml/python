from unittest.mock import patch, MagicMock
from copy import deepcopy
from time import time

import pytest
from collections import namedtuple
import pandas as pd
from sklearn.linear_model import LinearRegression
from sklearn.svm import SVC

import conML
from conML.shared.logger import ReconstructionInfo
from conML.domain.knowledge import  KnowledgeDatabase
from conML.shared.settings import ReconstructionSettings
from conML.domain.reconstruction.reconstruction import Metadata, Reconstructor, InterimPragmatic
from conML.ports.source_adapter import PandasBlock, build_block_from_rows
from conML.ports.ml_adapter import ReconstructionConceptualMLModel
from conML.ports.krippendorff_adapter import krippendorff_alpha
from conML.domain.reconstruction.selection import WinnerSelectionInfo
from conML.domain.reconstruction import (
    split,
    valid_reconstructed,
    PragmaticMachineLearningModel,
)


def get_labeled_block():
    df = pd.DataFrame(
        {
            "-1.0.1": [pow(i, 2) for i in range(10, 100)],
            "-1.0.2": [pow(i, 0.5) for i in range(10, 100)],
            "-1.0.3": [pow(i, 3) for i in range(10, 100)],
            "-1.0.4": [pow(i, 0.3) for i in range(10, 100)],
            "-1.0.5": [pow(i, 0.1) for i in range(10, 100)],
            "-1.0.6": [pow(i, 4) for i in range(10, 100)],
            "T": [int(time()) for i in range(10, 100)],
            "Sigma": ["" for i in range(10, 100)],
            "Z": [-1 if i < 50 else 1 for i in range(10, 100)]
        }
    )
    labeled = PandasBlock(df, relationship=("Z", "Sigma"), origin=("source", ))
    labeled.subject = "Kme01"
    labeled.n_cluster = 1
    labeled.origin = ("C.0.1", "C.1.2")
    return labeled


def test_learnblock_splitting_for_training():
    # act
    labeled = get_labeled_block()
    training_block, evaluation_block = split(labeled, 0.2)
    training_data_size = int(labeled.n_rows() * 0.8)
    evaluation_data_size = labeled.n_rows() - training_data_size

    # assert
    assert training_block.n_rows() == training_data_size
    assert evaluation_block.n_rows() == evaluation_data_size


def test_valid_reconstructed_true():
    # arrange
    model = MagicMock(spec=ReconstructionConceptualMLModel)
    model.accuracy = 0.9

    settings = ReconstructionSettings()

    # act
    actual_result = valid_reconstructed(model, "conceptual", settings)

    # assert
    assert actual_result is True


def test_valid_reconstructed_false():
    # arrange
    model = MagicMock(spec=ReconstructionConceptualMLModel)
    model.accuracy = 0.0

    settings = ReconstructionSettings()

    # act
    actual_result = valid_reconstructed(model, "conceptual", settings)

    # assert
    assert actual_result is False


@pytest.fixture()
def reconstructor():
    mode = "conceptual"
    m1 = MagicMock(spec=ReconstructionConceptualMLModel)
    m2 = MagicMock(spec=ReconstructionConceptualMLModel)
    algos = [m1, m2]
    settings = ReconstructionSettings()
    return Reconstructor(mode, algos, settings, krippendorff_alpha)


class TestMeteData:

    def test_eq(self):
        pandas_block = get_labeled_block()
        meta_data_1 = Metadata(
            knowledge_domain="C",
            knowledge_level=2,
            identifier=7,
            learnblock_indices=None,
            learnblock_labels=pandas_block.df["Z"],
            learnblock_features=list(pandas_block.df.columns),
            t_min=min(pandas_block.df["T"]),
            t_max=max(pandas_block.df["T"]),
            subject=("Svc",),
            aim=("Kme02",)
        )
        meta_data_2 = deepcopy(meta_data_1)

        # act
        assert meta_data_1 == meta_data_2
        with pytest.raises(TypeError):
            meta_data_1 == None

    def test_instantiation(self):
        pass

    def test_str_and_repr(self):
        # arrange
        pandas_block = get_labeled_block()
        meta_data = Metadata(
            knowledge_domain="C",
            knowledge_level=2,
            identifier=7,
            learnblock_indices=None,
            learnblock_labels=pandas_block.df["Z"],
            learnblock_features=list(pandas_block.df.columns),
            t_min=min(pandas_block.df["T"]),
            t_max=max(pandas_block.df["T"]),
            subject=("Svc",),
            aim=("Kme02",)
        )

        # act
        str_rep = str(meta_data)
        repr_rep = repr(meta_data)

        # assert
        assert str_rep == (
            "\n".join([
                "{:20}:{}".format("Knowledge Domain", "C"),
                "{:20}:{}".format("Knowledge Level", 2),
                "{:20}:{}".format("Identifier", 7),
                "{:20}:{}".format("Learnblock Indices", str(None)),
                "{:20}:{}".format("Learnblock Labels", pandas_block.df["Z"]),
                "{:20}:{}".format("Learnblock Features",
                                  list(pandas_block.df.columns)),
                "{:20}:{}".format("T Min", min(pandas_block.df["T"])),
                "{:20}:{}".format("T Max", max(pandas_block.df["T"])),
                "{:20}:{}".format("Subject", ("Svc",)),
                "{:20}:{}".format("Aim", ("Kme02",))
            ]
            )
        )

        assert repr_rep == (
            ", ".format([
                "{}={}".format("knowledge_domain", "C"),
                "{}={}".format("knowledge_level", 2),
                "{}={}".format("identifier", 7),
                "{}={}".format("learnblock_indices", str(None)),
                "{}={}".format("learnblock_features", pandas_block.df["Z"]),
                "{}={}".format("learnblock_labels", list(pandas_block.df.columns)),
                "{}={}".format("t_min", min(pandas_block.df["T"])),
                "{}={}".format("t_max", max(pandas_block.df["T"])),
                "{}={}".format("subject", ("Svc",)),
                "{}={}".format("aim", ("Kme02",)),
            ]
            )
        )


@pytest.fixture()
def pmlm():
    pandas_block = get_labeled_block()

    meta_data = Metadata(
        knowledge_domain="C",
        knowledge_level=2,
        identifier=7,
        learnblock_indices=None,
        learnblock_labels=pandas_block.df["Z"],
        learnblock_features=list(pandas_block.df.columns),
        t_min=min(pandas_block.df["T"]),
        t_max=max(pandas_block.df["T"]),
        subject=("Svc",),
        aim=("Kme02",)
    )

    svc = SVC()
    prag = PragmaticMachineLearningModel(meta_data, svc, pandas_block)
    prag.set_subjects(["Svc", "Tree"])
    return prag


class TestPragmaticMachineLearningModel:

    def test_str_and_repr(self, pmlm):
        # act
        str_rep = str(pmlm)
        repr_rep = repr(pmlm)

        # assert
        assert str_rep == (
            "\n".join([
                str(pmlm.meta),
                "{:20}: {}".format("Origin", str(pmlm.origin))
            ]
            )
        )

        assert repr_rep == (
            ", ".format([
                repr(pmlm),
                "{}={}".format("Origin", str(pmlm.origin)),
                "{}=[{}]".format("model", repr(pmlm.model))
            ]
            )
        )

    def test_eq(self, pmlm):
        # arrange
        m2 = deepcopy(pmlm)

        # act/assert
        with pytest.raises(NotImplementedError):
            pmlm == None

        assert pmlm == m2

    def test_fusion(self, pmlm):
        # arrange
        new_identifier = 666
        m = MagicMock(spec=PragmaticMachineLearningModel)
        m.min_timestamp = pmlm.min_timestamp - 1
        m.max_timestamp = pmlm.max_timestamp + 1
        m.subject = ("LiR2", )
        m.subjects = ("LiR3", "LiR4")
        m.aim = ("Lir203", )

        # act
        actual_result = pmlm.fusion(m, 1, new_identifier)

        # assert
        assert actual_result.t_min == m.min_timestamp
        assert actual_result.t_max == m.max_timestamp
        assert actual_result.subject[0] in ("Svc", "LiR2")
        assert actual_result.subject[1] in ("Svc", "LiR2")
        assert actual_result.aim[0] in ["Kme02", "Lir203"]
        assert actual_result.aim[1] in ["Kme02", "Lir203"]
        for j in actual_result.subjects:
            assert j in m.subjects+pmlm.subjects

    def test_trained_with_on_higher_levels(self, pmlm):
        # arrange
        kdb = MagicMock(spec=KnowledgeDatabase)
        block = MagicMock(spec=MagicMock)
        pmlm._PragmaticMachineLearningModel__learnblock = block

        # act
        actual_result = pmlm.trained_with(kdb)

        # assert
        assert actual_result == block

    def test_trained_with_on_level_one(self, pmlm):
        # arrange
        block = MagicMock(spec=PandasBlock)
        block.columns.return_value = ["0.0.1", "0.0.2"]
        kdb = MagicMock(spec=KnowledgeDatabase)
        kdb.get_block.return_value = block
        pmlm._PragmaticMachineLearningModel__learnblock = None
        pmlm.meta.learnblock_features = ["0.0.1", "0.0.2"]

        # act
        actual_result = pmlm.trained_with(kdb)

        # assert
        assert actual_result.subject == pmlm.subject
        assert actual_result.origin == pmlm.origin
        assert actual_result is block

    def test_set_subjects(self, pmlm):
        # arrange
        subects = ("LiR", )

        # act
        pmlm.set_subjects(subects)

        # assert
        assert pmlm.subjects == subects


class TestReconstructor:

    def test_str_and_repr(self, reconstructor):
        # act
        str_rep = str(reconstructor)
        repr_rep = repr(reconstructor)

        # assert
        assert str_rep == (
            "\n".join([
                "{:20}: {}".format("Mode", "conceptual"),
                "{:20}: {}".format("ML Models", [
                    str(m) for m in reconstructor.ml_models]),
                str(reconstructor.settings)
            ])
        )

        assert repr_rep == (
            ", ".join([
                "{}={}".format("mode", "conceptual"),
                "{}".format(repr(reconstructor.settings)),
                "{}={}".format("ml_models", repr(reconstructor.ml_models))
            ])
        )

    @patch(
        "conML.domain.reconstruction.selection.WinnerSelector.determine_winner")
    def test_select_winner(self, mock_winner, reconstructor):
        # arrange
        log = MagicMock(spec=WinnerSelectionInfo)
        winner = MagicMock(spec=PragmaticMachineLearningModel)
        reliablity_dict = {0.7: [winner]}
        mock_winner.return_value = log, winner

        # act
        actual_result = reconstructor.select_winner(reliablity_dict)

        # assert
        assert actual_result[0] == log
        assert actual_result[1] == winner

    def test_reconstruct_failure_interim_zero_length(self, reconstructor):
        # arrange
        lb = get_labeled_block()
        logs = [MagicMock(spec=ReconstructionInfo),
                MagicMock(spec=ReconstructionInfo)]
        interims = []

        reconstructor.build_interim_pragmatics = lambda *args: (interims, logs)

        # act
        r, m, l = reconstructor.reconstruct(1, lb, which_models=None, meta=None)

        # assert
        assert r == 0.0
        assert m == []
        assert l == logs

    def test_reconstruct_failure_reliability_to_small(self, reconstructor):
        # arrange
        lb = get_labeled_block()
        logs = [MagicMock(spec=ReconstructionInfo),
                MagicMock(spec=ReconstructionInfo)]
        interims = [MagicMock(spec=InterimPragmatic),
                    MagicMock(spec=InterimPragmatic)]

        reconstructor.min_reliability = 0.8
        reconstructor.calc_intersubject_reliability = lambda *args: 0.1
        reconstructor.build_interim_pragmatics = lambda *args: (interims, logs)

        # act
        r, m, l = reconstructor.reconstruct(1, lb, which_models=None, meta=None)

        # assert
        assert r == 0.0
        assert l[0].reliability == 0.1
        assert l[1].reliability == 0.1
        assert m == []

    def test_reconstruct_success(self, reconstructor):
        # arrange
        lb = get_labeled_block()
        logs = [MagicMock(spec=ReconstructionInfo),
                MagicMock(spec=ReconstructionInfo)]
        interims = [MagicMock(spec=InterimPragmatic),
                    MagicMock(spec=InterimPragmatic)]

        reconstructor.calc_intersubject_reliability = lambda *args: 0.9
        reconstructor.build_interim_pragmatics = lambda *args: (interims, logs)

        # act
        r, m, l = reconstructor.reconstruct(1, lb, which_models=None, meta=None)

        # assert
        assert r == 0.9
        assert l[0].reliability == 0.9
        assert l[1].reliability == 0.9
        assert m[0].reliability == 0.9
        assert m[1].reliability == 0.9

    @patch("conML.domain.reconstruction.reconstruction.PragmaticMachineLearningModel")
    @patch("conML.domain.reconstruction.reconstruction.valid_reconstructed")
    @patch("conML.domain.reconstruction.reconstruction.split")
    def test_build_interim_pragmatics(self, mock_split, mock_valid,
                                      mock_pragmatic, reconstructor):
        # arrange
        lb = get_labeled_block()

        train_mock = MagicMock(spec=PandasBlock)
        train_mock.get_column.return_value = [1, 2, 1, 2]
        train_mock.as_numpy_array.return_value = []

        test_mock = MagicMock(spec=PandasBlock)
        test_mock.get_column.return_value = [1, 2, 1, 2]
        test_mock.as_numpy_array.return_value = []

        mock_split.return_value = train_mock, test_mock

        ml_models = [MagicMock(spec=ReconstructionConceptualMLModel),
                     MagicMock(spec=ReconstructionConceptualMLModel)]
        ml_models[0].score.return_value = 0.1
        ml_models[1].score.return_value = 0.2

        reconstructor.machine_learning_models = lambda *args: ml_models
        reconstructor.build_meta = lambda *args: MagicMock(spec=Metadata)

        mock_valid.side_effects = [True, False]

        # act
        m, l = reconstructor.build_interim_pragmatics(1, lb)

        # assert
        assert isinstance(m, list)
        assert isinstance(l, list)

    def test_machine_learnign_models_unspecified(self, reconstructor):
        # act
        result = list(reconstructor.machine_learning_models(which_models=None))

        # assert
        assert result == [reconstructor.ml_models[0], reconstructor.ml_models[1]]

    def test_machine_learning_models_specified(self, reconstructor):
        # act
        reconstructor.ml_models[0].subject = "LiR1"
        reconstructor.ml_models[1].subject = "LiR"
        result = list(reconstructor.machine_learning_models(which_models=["LiR"]))

        # assert
        assert result == [reconstructor.ml_models[1]]

    def test_calc_intersubject_reliability_returns_zero(self, reconstructor):
        # arrange
        Interim = namedtuple("Interim", "predictions")

        # act
        actual_result = reconstructor.calc_intersubject_reliability([
            Interim([1, 1, 1, 0])
        ])

        # assert
        assert actual_result == 0.0

    def test_calc_intersubject_reliability_returns_non_zero(self, reconstructor):
        # arrange
        Interim = namedtuple("Interim", "predictions")
        reconstructor.krippendorff_alpha = lambda *args: 1.0

        # act
        actual_result = reconstructor.calc_intersubject_reliability([
            Interim([1, 1, 1, 0]), Interim([1, 1, 1, 0])
        ])

        # assert
        assert actual_result == 1.0

    def test_build_meta_on_level_one(self, reconstructor):
        # arrange
        reconstructor.identifier_queue.put(101)
        lb = get_labeled_block()
        reconstructor.build_aim = lambda *args: "C.1.Kme02"
        reconstructor.ml_models[0].subject = "Svc"

        # act
        actual_result = reconstructor.build_meta(
            1, lb, reconstructor.ml_models[0], meta=None
        )

        # assert
        assert actual_result.knowledge_domain == "C"
        assert actual_result.knowledge_level == 1
        assert actual_result.identifier == 101
        assert actual_result.t_min == lb.min_timestamp()
        assert actual_result.t_max == lb.max_timestamp()
        assert actual_result.aim == ("C.1.Kme02",)
        assert actual_result.subject == ("Svc",)
        assert actual_result.learnblock_features == lb.columns()
        assert actual_result.learnblock_labels == list(lb.df["Z"].values)
        assert actual_result.learnblock_indices == list(lb.df.index.values)

    def test_build_meta_on_higher_levels(self, reconstructor):
        # arrange
        reconstructor.identifier_queue.put(101)
        lb = MagicMock(spec=PandasBlock)
        lb.indices.return_value = [0, 1, 2, 3]
        lb.columns.return_value = ["0.0.1", "0.0.2"]
        lb.get_column.return_value = [1, 2, 1, 2]
        reconstructor.build_aim = lambda *args: "C.1.Kme02"
        reconstructor.ml_models[0].subject = "Svc"
        meta = MagicMock(spec=Metadata)

        # act
        actual_result = reconstructor.build_meta(
            1, lb, reconstructor.ml_models[0], meta=meta
        )

        # assert
        assert actual_result is meta
        assert actual_result.learnblock_labels == [1, 2, 1, 2]
        assert actual_result.learnblock_indices == [0, 1, 2, 3]
        assert actual_result.learnblock_features == ["0.0.1", "0.0.2"]

    def test_build_aim(self, reconstructor):
        # arrange
        block = get_labeled_block()
        block.subject = "Kme02"

        # act
        result = reconstructor.build_aim(2, block)

        # assert
        assert result == "C.2.Kme02"

    def test_get_pml(self, reconstructor):
        # arrange
        meta= MagicMock(spec=Metadata)
        block = MagicMock(spec=PandasBlock)
        ml_models = [
            ReconstructionConceptualMLModel(SVC(), "Svc"),
            ReconstructionConceptualMLModel(LinearRegression(), "LiR")
        ]
        block.indices.return_value = [1, 2, 3, 4]
        block.columns.return_value = ["0.0.1", "0.0.2"]
        block.get_column.return_value = [1, 2, 1, 2]

        actual_result = reconstructor.get_pml(meta, block, ml_models)

        assert actual_result.meta.learnblock_indicies == [1, 2, 3, 4]
        assert actual_result.meta.learnblock_features == ["0.0.1", "0.0.2"]
        assert actual_result.meta.learnblock_labels == [1, 2, 1, 2]
