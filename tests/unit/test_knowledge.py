import os
import tempfile
from datetime import datetime
from time import time

import pytest
import pandas as pd
from sklearn.svm import SVC

import conML
from conML.domain.knowledge import (
    KnowledgeDatabase,
    KnowledgeDomain,
    KnowledgeSource,
    RelativeFinder)
from conML.domain.reconstruction.reconstruction import (
    Metadata, PragmaticMachineLearningModel)
from conML.ports.source_adapter import build_block_from_rows, PandasBlock


def get_labeled_block():
    df = pd.DataFrame(
        {
            "0.0.1": [pow(i, 2) for i in range(10, 100)],
            "0.0.2": [pow(i, 0.5) for i in range(10, 100)],
            "T": [int(time()) for i in range(10, 100)],
            "Sigma": ["" for i in range(10, 100)],
            "Z": [0 if i < 50 else 1 for i in range(10, 100)]
        }
    )
    labeled = PandasBlock(df, relationship=("Z", "Sigma"), origin=("source", ))
    labeled.subject = "Kme02"
    labeled.n_cluster = 2
    labeled.origin = ("C.1.1", "C.1.2")
    return labeled


def get_pmlm():
    pandas_block = get_labeled_block()

    meta_data = Metadata(
        knowledge_domain="C",
        knowledge_level=2,
        identifier=7,
        learnblock_indices=None,
        learnblock_labels=pandas_block.df["Z"],
        learnblock_features=list(pandas_block.df.columns),
        t_min=min(pandas_block.df["T"]),
        t_max=max(pandas_block.df["T"]),
        subject=("Svc",),
        aim=("Kme02",)
    )

    svc = SVC(gamma="scale")
    prag = PragmaticMachineLearningModel(meta_data, svc, pandas_block)
    prag.set_subjects(["Svc", "Tree"])
    return prag


class TestKnowledgeDatabase:

    def test_str_and_repr(self):
        # arrange
        knowledge = KnowledgeDatabase(2, build_block_from_rows)
        expected_str_result = "\n".join([
            "{:20}: {}".format("# Blocks", 0),
            "{:*^100}" .format("Level 0"),
            "{:20}: {}".format("Models", []),
            "{:*^100}".format("Level 1"),
            "{:20}: {}".format("Models", []),
            "{:*^100}".format("Level 2"),
            "{:20}: {}".format("Models", []),
        ])

        expected_repr_result = (
            "N_models=0, Source=Blocks=0, Sources={} "
            "[KnowledgeDomain=Level=0, Models=[], Biggest ID=0] "
            "[KnowledgeDomain=Level=1, Models=[], Biggest ID=0] "
            "[KnowledgeDomain=Level=2, Models=[], Biggest ID=0]"
        )

        # act
        str_rep = str(knowledge)
        repr_rep = repr(knowledge)

        # assert
        assert str_rep == expected_str_result
        assert repr_rep == expected_repr_result

    def test_save(self):
        # arrange
        knowledge = KnowledgeDatabase(7, build_block_from_rows)
        knowledge.database[6].biggest = 10

        m1 = get_pmlm()
        m1.meta.knowledge_level = 7
        knowledge.insert(m1)

        m2 = get_pmlm()
        m2.meta.identifier = 666
        knowledge.insert(m2)

        # act/assert
        with tempfile.TemporaryDirectory() as fp:
            backup = os.path.join(fp, "knowledge")
            knowledge.save(backup)
            loaded = conML.load_knowledge(backup)

            assert loaded.database[6].biggest == 10
            assert loaded.observer == []
            assert loaded.database[7].knowledge[m1]
            assert loaded.database[2].knowledge[m2]

    def test_instantiation(self):
        # act
        knowledge = KnowledgeDatabase(7, build_block_from_rows)

        # assert
        assert len(knowledge.database) == 8
        assert knowledge.n_models == 0
        assert knowledge.highest_level == 7

    def test_extend(self):
        # arrange
        knowledge = KnowledgeDatabase(7, build_block_from_rows)
        block = get_labeled_block()

        # act
        knowledge.extend(block)

        # assert
        indices = [i for i in range(90)]
        saved_block = knowledge.source.get(indices)
        assert saved_block.n_rows() == block.n_rows()
        assert saved_block.n_columns() == saved_block.n_columns()

    def test_get_block(self):
        # arrange
        knowledge = KnowledgeDatabase(7, build_block_from_rows)
        block = get_labeled_block()
        knowledge.extend(block)

        # act
        indices = [i for i in range(90)]
        saved_block = knowledge.get_block(indices)

        # assert
        assert saved_block.n_rows() == block.n_rows()
        assert saved_block.n_columns() == block.n_columns()
        assert saved_block.get_column("0.0.1") == block.get_column("0.0.1")
        assert saved_block.get_column("0.0.2") == block.get_column("0.0.2")
        assert saved_block.get_column("Z") == block.get_column("Z")
        assert saved_block.get_column("T") == block.get_column("T")
        assert saved_block.get_column("Sigma") == block.get_column("Sigma")

    def test_insert_model(self):
        # arrange
        knowledge = KnowledgeDatabase(7, build_block_from_rows)
        model = get_pmlm()

        # act
        knowledge.insert(model)

        # assert
        assert len(knowledge.database[2].knowledge) == 1
        assert knowledge.database[2].knowledge[model.uid] == model
        assert knowledge.n_models == 1

    def test_remove_model(self):
        # arrange
        knowledge = KnowledgeDatabase(7, build_block_from_rows)
        model = get_pmlm()
        knowledge.insert(model)

        # act
        knowledge.remove(model)

        # assert
        assert len(knowledge.database[2].knowledge) == 0
        assert knowledge.n_models == 0

        try:
            knowledge.get_model(model.uid)
            assert False
        except KeyError:
            assert True

    def test_get_model_success(self):
        # arrange
        knowledge = KnowledgeDatabase(7, build_block_from_rows)
        model = get_pmlm()
        knowledge.insert(model)

        # act
        actual_result = knowledge.get_model("C.2.7")

        # assert
        assert actual_result == model

    def test_get_model_failure(self):
        # arrange
        knowledge = KnowledgeDatabase(7, build_block_from_rows)

        # act, assert
        try:
            knowledge.get_model("C.2.7")
            assert False
        except KeyError:
            assert True

    def test_remove_dependent_models(self):
        # arrange
        knowledge = KnowledgeDatabase(7, build_block_from_rows)
        one = get_pmlm()
        one.origin = ("C.1.7", "C.1.6")
        knowledge.insert(one)

        two = get_pmlm()
        two.meta.identifier = 7
        two.meta.knowledge_level = 1
        knowledge.insert(two)

        three = get_pmlm()
        three.meta.identifier = 6
        three.meta.knowledge_level = 1
        knowledge.insert(three)

        # act
        dependent = list(knowledge.remove_dependent(two))

        # assert
        assert one in dependent


class TestKnowledgeDomain:

    def test_get(self):
        # arrange
        domain = KnowledgeDomain(5)
        model = get_pmlm()
        domain.knowledge[model] = model

        # act
        actual_result = domain.get(model)

        # assert
        assert actual_result == model

    def test_insert(self):
        # arrange
        domain = KnowledgeDomain(5)
        model = get_pmlm()

        # act
        domain.insert(model)

        # assert
        assert domain.get(model) == model
        assert len(domain.knowledge) == 1

    def test_remove(self):
        # arrange
        domain = KnowledgeDomain(5)
        model = get_pmlm()
        model.meta.knowledge_level = 5

        domain.insert(model)

        # act
        domain.remove(model)

        # assert
        assert len(domain.knowledge) == 0

    def test_next_id(self):
        # arrange
        domain = KnowledgeDomain(5)

        # act
        actual_res_1 = domain.next_id
        actual_res_2 = domain.next_id
        actual_res_3 = domain.next_id

        # assert
        assert actual_res_1 == 1
        assert actual_res_2 == 2
        assert actual_res_3 == 3
        assert domain.biggest_id == 3


class TestKnowledgeSource:

    def test_repr(self):
        # arrange
        source = KnowledgeSource(build_block_from_rows)
        block = get_labeled_block()
        source.append(block)
        expected_result = (
            "Blocks=1, Sources={0: {'range': (0, 90), 'shape': (90, 5)}}"
        )

        # act/assert
        repr_rep = repr(source)
        assert repr_rep == expected_result

        source.append(get_labeled_block())
        expected_result = (
            "Blocks=2, Sources={0: {'range': (0, 90), 'shape': (90, 5)}, "
            "1: {'range': (90, 180), 'shape': (90, 5)}}"
        )
        repr_rep = repr(source)
        assert repr_rep == expected_result


    def test_append_one_block(self):
        # arrange
        source = KnowledgeSource(build_block_from_rows)
        block = get_labeled_block()

        # act
        n_block = source.append(block)

        # assert
        assert n_block == 0
        assert source.source[0] == block
        for idx in range(90):
            assert source.bucket_search(idx) == 0
        assert source._KnowledgeSource__search_ranges == [90]
        assert len(source.source) == 1
        assert source._KnowledgeSource__last_index == 90

    def test_append_three_blocks(self):
        # arrange
        source = KnowledgeSource(build_block_from_rows)
        block_1 = get_labeled_block()
        block_2 = get_labeled_block()
        block_3 = get_labeled_block()

        # act
        n_block_1 = source.append(block_1)
        n_block_2 = source.append(block_2)
        n_block_3 = source.append(block_3)

        # assert
        assert n_block_1 == 0
        assert n_block_2 == 1
        assert n_block_3 == 2

        assert source.source[0] == block_1
        assert source.source[1] == block_2
        assert source.source[2] == block_3
        assert source._KnowledgeSource__search_ranges == [90, 180, 270]
        assert len(source.source)
        assert source._KnowledgeSource__last_index == 270

    def test_get_block(self):
        # arrange
        source = KnowledgeSource(build_block_from_rows)
        block = get_labeled_block()
        source.append(block)

        # act
        indices = [i for i in range(90)]
        actual_result = source.get(indices)

        # assert
        for i, j in zip(actual_result, block):
            assert i[0] == j[0]
            assert i[1] == j[1]

    def test_reindex(self):
        # arrange
        source = KnowledgeSource(build_block_from_rows)
        block_1 = get_labeled_block()
        block_2 = get_labeled_block()

        # act/assert
        source._KnowledgeSource__reindex(block_1)
        assert source._KnowledgeSource__last_index == 90
        assert block_1.indices() == [i for i in range(90)]
        source._KnowledgeSource__reindex(block_2)
        assert source._KnowledgeSource__last_index == 180
        assert block_2.indices() == [i for i in range(90, 180)]

    def test_update_search_range(self):
        # arrange
        source = KnowledgeSource(build_block_from_rows)
        source._KnowledgeSource__last_index = 90

        # act
        source._KnowledgeSource__update_search_range()

        # assert
        assert source._KnowledgeSource__search_ranges == [90]

    def test_indices_to_rows(self):
        # arrange
        source = KnowledgeSource(build_block_from_rows)
        block = get_labeled_block()
        source.append(block)

        # act
        indices = [i for i in range(90)]
        rows = list(source.indices_to_rows(indices))

        # assert
        for row, j in zip(rows, range(90)):
            assert row["0.0.1"].values[0] == block.df["0.0.1"].iloc[j]
            assert row["0.0.2"].values[0] == block.df["0.0.2"].iloc[j]
            assert row["T"].values[0] == block.df["T"].iloc[j]
            assert row["Sigma"].values[0] == block.df["Sigma"].iloc[j]
            assert row["Z"].values[0] == block.df["Z"].iloc[j]

    def test_bucket_search(self):
        # arrange
        source = KnowledgeSource(build_block_from_rows)
        source._KnowledgeSource__search_ranges = [90, 180, 270]

        # act
        idx_1 = source.bucket_search(0)
        idx_2 = source.bucket_search(90)
        idx_3 = source.bucket_search(180)
        idx_4 = source.bucket_search(150)

        # assert
        assert idx_1 == 0
        assert idx_2 == 1
        assert idx_3 == 2
        assert idx_4 == 1

    def test_bucket_search_throws_exception(self):
        from conML.shared.errors import IndexOutOfBucket

        # arrange
        source = KnowledgeSource(build_block_from_rows)

        # act/assert
        with pytest.raises(IndexOutOfBucket):
            source.bucket_search(80)


class TestRelativeFinder:

    def test_contains_returns_true(self):
        # arrange
        relative_finder = RelativeFinder(7)
        model_1 = get_pmlm()
        model_1.meta.identifier = 1
        model_1.meta.subject = ("Svc", )
        model_1.meta.t_min = int(datetime.strptime("2010", "%Y").timestamp())
        model_1.meta.t_max = int(datetime.strptime("2020", "%Y").timestamp())
        relative_finder.insert(model_1)

        # act
        actual_result = model_1 in relative_finder

        # assert
        assert actual_result is True

    def test_contains_returns_false(self):
        # arrange
        relative_finder = RelativeFinder(7)
        model_1 = get_pmlm()

        # act
        actual_result = model_1 in relative_finder

        # assert
        assert actual_result is False

    def test_find_t_sigma(self):
        # arrange
        relative_finder = RelativeFinder(7)
        model_1 = get_pmlm()
        model_1.meta.identifier = 1
        model_1.meta.subject = ("Svc", )
        model_1.meta.t_min = int(datetime.strptime("2010", "%Y").timestamp())
        model_1.meta.t_max = int(datetime.strptime("2020", "%Y").timestamp())
        relative_finder.insert(model_1)

        model_2 = get_pmlm()
        model_2.meta.identifier = 2
        model_2.meta.subject = ("Svc", )
        model_2.meta.t_min = int(datetime.strptime("2010", "%Y").timestamp())
        model_2.meta.t_max = int(datetime.strptime("2020", "%Y").timestamp())
        relative_finder.insert(model_2)

        pragmatic = get_pmlm()
        pragmatic.meta.subject = ("Svc", )
        pragmatic.meta.t_min = int(datetime.strptime("2010", "%Y").timestamp())
        pragmatic.meta.t_max = int(datetime.strptime("2020", "%Y").timestamp())

        # act
        relatives = relative_finder.find(("T", "Sigma"), pragmatic,
                                         deconst_full_tolerance=0.1)

        # assert
        print(relative_finder.indices)

        assert model_1 in relatives
        assert model_2 in relatives

    def test_find_sigma_z(self):
        # arrange
        relative_finder = RelativeFinder(7)
        model_1 = get_pmlm()
        model_1.meta.identifier = 1
        model_1.meta.subject = ("Svc", )
        relative_finder.insert(model_1)

        model_2 = get_pmlm()
        model_2.meta.identifier = 2
        model_2.meta.subject = ("Svc", )
        relative_finder.insert(model_2)

        pragmatic = get_pmlm()
        pragmatic.meta.subject = ("Svc", )

        # act
        relatives = relative_finder.find(("Sigma", "Z"), pragmatic, 0.1)

        # assert
        assert model_1 in relatives
        assert model_2 in relatives

    def test_find_complete_with_zero_tolerance(self):
        # arrange
        relative_finder = RelativeFinder(7)

        model_1 = get_pmlm()
        model_1.meta.identifier = 1
        model_1.meta.subject = ("Svc", )
        model_1.meta.t_min = int(datetime.strptime("2010", "%Y").timestamp())
        model_1.meta.t_max = int(datetime.strptime("2020", "%Y").timestamp())
        relative_finder.insert(model_1)

        model_2 = get_pmlm()
        model_2.meta.identifier = 2
        model_2.meta.subject = ("Svc", )
        model_2.meta.t_min = int(datetime.strptime("2010", "%Y").timestamp())
        model_2.meta.t_max = int(datetime.strptime("2020", "%Y").timestamp())
        relative_finder.insert(model_2)

        pragmatic = get_pmlm()
        pragmatic.meta.subject = ("Svc", )
        pragmatic.meta.t_min = int(datetime.strptime("2010", "%Y").timestamp())
        pragmatic.meta.t_max = int(datetime.strptime("2010", "%Y").timestamp())
        relative_finder.insert(model_2)

        # act
        relatives = relative_finder.find(("complete", ), pragmatic, 0.0)

        # assert
        assert model_1 in relatives
        assert model_2 in relatives

    def test_find_complete_with_10_percent_tolerance(self):
        # arrange
        relative_finder = RelativeFinder(7)

        model_1 = get_pmlm()
        model_1.meta.identifier = 1
        model_1.meta.subject = ("Svc", )
        model_1.meta.t_min = int(datetime.strptime("2010", "%Y").timestamp())
        model_1.meta.t_max = int(datetime.strptime("2020", "%Y").timestamp())
        relative_finder.insert(model_1)

        model_2 = get_pmlm()
        model_2.meta.identifier = 2
        model_2.meta.subject = ("Svc", )
        model_2.meta.t_min = int(datetime.strptime("2010", "%Y").timestamp())
        model_2.meta.t_max = int(datetime.strptime("2020", "%Y").timestamp())
        relative_finder.insert(model_2)

        pragmatic = get_pmlm()
        pragmatic.meta.subject = ("Svc", )
        pragmatic.meta.t_min = int(datetime.strptime("2010", "%Y").timestamp())
        pragmatic.meta.t_max = int(datetime.strptime("2020", "%Y").timestamp())
        relative_finder.insert(model_2)

        # act
        relatives = relative_finder.find(("complete", ), pragmatic, 0.1)

        # assert
        assert model_1 in relatives
        assert model_2 in relatives

    def test_remove(self):
        # arrange
        relative_finder = RelativeFinder(7)
        model_1 = get_pmlm()
        model_1.meta.identifier = 1
        model_1.meta.subject = ("Svc", )
        model_1.meta.t_min = int(datetime.strptime("2010", "%Y").timestamp())
        model_1.meta.t_max = int(datetime.strptime("2020", "%Y").timestamp())
        relative_finder.insert(model_1)

        print(relative_finder.__repr__(2))
        # act
        relative_finder.remove(model_1)
        print(relative_finder.__repr__(2))

        # assert
        assert len(relative_finder.indices[2][0]) == 0
        assert len(relative_finder.indices[2][1]) == 0
        assert len(relative_finder.indices[2][2]) == 2

    def test_insert(self):
        # arrange
        relative_finder = RelativeFinder(7)
        model_1 = get_pmlm()
        model_1.meta.identifier = 10
        model_1.meta.subject = ("Svc", )
        model_1.meta.t_min = int(datetime.strptime("2010", "%Y").timestamp())
        model_1.meta.t_max = int(datetime.strptime("2020", "%Y").timestamp())

        # act
        relative_finder.insert(model_1)

        # assert
        assert model_1 in relative_finder.indices[model_1.level][0][
            (model_1.min_timestamp, model_1.max_timestamp)
        ]
        assert model_1 in relative_finder.indices[model_1.level][1][
            model_1.aim
        ]
        assert model_1.uid in relative_finder.indices[model_1.level][2][
            model_1.subject[0]
        ]

    def test_get_index_lists(self):
        # arrange
        relative_finder = RelativeFinder(7)
        model_1 = get_pmlm()
        model_1.meta.identifier = 10
        model_1.meta.subject = ("Svc", )
        model_1.meta.t_min = int(datetime.strptime("2010", "%Y").timestamp())
        model_1.meta.t_max = int(datetime.strptime("2020", "%Y").timestamp())
        relative_finder.insert(model_1)

        model_2 = get_pmlm()
        model_2.meta.identifier = 11
        model_2.meta.subject = ("Svc", )
        model_2.meta.t_min = int(datetime.strptime("2010", "%Y").timestamp())
        model_2.meta.t_max = int(datetime.strptime("2020", "%Y").timestamp())
        relative_finder.insert(model_2)

        # act
        t, sigma, z = relative_finder.get_index_lists(
            model_1, time=True, subject=True, aim=True)

        # assert
        assert model_1 in t and model_2 in t
        assert model_1 in z and model_2 in z
        assert model_1 in sigma[0] and model_2 in sigma[1]

    def test_get_index_level(self):
        # arrange
        relative_finder = RelativeFinder(7)
        model_1 = get_pmlm()
        model_1.meta.identifier = 10
        model_1.meta.subject = ("Svc", )
        model_1.meta.t_min = int(datetime.strptime("2010", "%Y").timestamp())
        model_1.meta.t_max = int(datetime.strptime("2020", "%Y").timestamp())
        relative_finder.insert(model_1)

        model_2 = get_pmlm()
        model_2.meta.identifier = 11
        model_2.meta.subject = ("Svc", )
        model_2.meta.t_min = int(datetime.strptime("2010", "%Y").timestamp())
        model_2.meta.t_max = int(datetime.strptime("2022", "%Y").timestamp())
        relative_finder.insert(model_2)

        # act
        t, z, s = relative_finder.get_index_level(model_2)

        # assert
        assert len(t) == 2
        assert len(z) == 1
        assert len(s) == 2
        assert model_1 in t[(model_1.min_timestamp, model_1.max_timestamp)]
        assert model_2 in t[(model_2.min_timestamp, model_2.max_timestamp)]
        assert model_1 in z[model_1.aim] and model_2 in z[model_2.aim]
        assert model_1 in s[model_1.subject[0]] and model_2 in s[model_2.subject[0]]
