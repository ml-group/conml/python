from time import time
from unittest.mock import MagicMock

import pytest
import pandas as pd
from sklearn.feature_selection import VarianceThreshold, SelectFromModel
from sklearn.ensemble import ExtraTreesClassifier

from conML.domain.reconstruction.selection import FeatureSelector
from conML.domain.reconstruction.reconstruction import PragmaticMachineLearningModel
from conML.domain.reconstruction.selection import WinnerSelector
from conML.shared.settings import FeatureSelectionSettings
from conML.shared.logger import SelectionInfo, WinnerSelectionInfo
from conML.ports.source_adapter import PandasBlock
from conML.ports import ml_adapter


@pytest.fixture()
def feature_selector():
    variance = ml_adapter.FilterMethod(VarianceThreshold(2000))
    forest = ml_adapter.EmbeddedMethod(
        SelectFromModel(ExtraTreesClassifier(n_estimators=10)))
    return FeatureSelector(
        filter_method=variance,
        embedded_method=forest,
        settings=FeatureSelectionSettings())


def get_labeled_block():
    df = pd.DataFrame(
        {
            "0.0.1": [pow(i, 2) for i in range(10, 100)],
            "0.0.2": [pow(i, 0.5) for i in range(10, 100)],
            "0.0.3": [pow(i, 3) for i in range(10, 100)],
            "0.0.4": [pow(i, 0.3) for i in range(10, 100)],
            "0.0.5": [pow(i, 0.1) for i in range(10, 100)],
            "0.0.6": [pow(i, 4) for i in range(10, 100)],
            "T": [int(time()) for i in range(10, 100)],
            "Sigma": ["" for i in range(10, 100)],
            "Z": [0 if i < 50 else 1 for i in range(10, 100)]
        }
    )
    labeled = PandasBlock(df, relationship=("Z", "Sigma"), origin=("source", ))
    labeled.subject = "Kme02"
    labeled.n_cluster = 2
    labeled.origin = ("C.1.1", "C.1.2")
    return labeled


class TestFeatureSelector:

    def test_filtering(self, feature_selector):
        # arrange
        lb = get_labeled_block()
        reduced_lb = MagicMock(spec=PandasBlock)
        filter_mock = MagicMock(spec=ml_adapter.FilterMethod)
        filter_mock.reduce.return_value = reduced_lb
        feature_selector.filter_method = filter_mock

        # act
        actual_result = feature_selector.filtering(lb)

        # assert
        assert actual_result == reduced_lb
        assert filter_mock.train.called == True

    def test_embedding(self, feature_selector):
        # arrange
        lb = get_labeled_block()
        reduced_lb = MagicMock(spec=PandasBlock)
        embedded_mock = MagicMock(spec=ml_adapter.FilterMethod)
        embedded_mock.reduce.return_value = reduced_lb
        feature_selector.embedded_method = embedded_mock

        # act
        actual_result = feature_selector.embedding(lb)

        # assert
        assert actual_result == reduced_lb
        assert embedded_mock.train.called == True

    def test_str_and_repr(self, feature_selector):
        # arrange
        settings = FeatureSelectionSettings()

        # act
        feature_selector_as_str = str(feature_selector)
        feature_selector_as_repr = repr(feature_selector)

        # assert
        assert feature_selector_as_str == "\n".join([
            "{:20}: {}".format("Filter Method", str(feature_selector.filter_method)),
            "{:20}: {}".format("Embedded Method", str(feature_selector.embedded_method)),
            str(settings)])

        assert feature_selector_as_repr == ", ".join([
            "{}={}".format("Filter Method", str(feature_selector.filter_method)),
            "{}={}".format("Embedded Method", str(feature_selector.embedded_method)),
            repr(settings)
            ]
        )

    def test_filter_method_criteria(self, feature_selector):
        # arrange
        feature_selector.settings.max_filter_x = 15
        feature_selector.settings.max_filter_y = 20
        learn_cols = 14
        samples_count = 151

        # act
        actual_result_one = feature_selector._filter_method_criteria(
            learn_cols, samples_count
        )

        learn_cols = 20
        actual_result_two = feature_selector._filter_method_criteria(
            learn_cols, samples_count
        )

        # assert
        assert actual_result_one is True
        assert actual_result_two is True

    def test_to_many_features(self, feature_selector):
        # arrange
        feature_selector.settings.max_features = 100

        # act
        actual_result = feature_selector._to_many_features(120)

        # assert
        assert actual_result is True

    def test_non_reducible_invalid_leanblock_returns_true(self, feature_selector):
        # arrange
        feature_selector.settings.max_features = 5
        past_n_features = 10
        lb = MagicMock(spec=PandasBlock)
        lb.n_columns.return_value = 10
        feature_selector._to_many_features = lambda *args: True

        # act
        actual_result = feature_selector.none_reducible_invalid_learnblock(
            past_n_features, lb
        )

        # assert
        assert actual_result is True

    def test_non_reducible_invalid_leanblock_returns_false(self, feature_selector):
        # arrange
        feature_selector.settings.max_features = 12
        past_n_features = 10
        lb = MagicMock(spec=PandasBlock)
        lb.n_columns.return_value = 10
        feature_selector._to_many_features = lambda *args: False

        # act
        actual_result = feature_selector.none_reducible_invalid_learnblock(
            past_n_features, lb
        )

        # assert
        assert actual_result is False

    def test_none_reducible_valid_learnblock_returns_true(self, feature_selector):
        # arrange
        feature_selector.settings.max_features = 11
        past_n_features = 10
        lb = MagicMock(spec=PandasBlock)
        lb.n_columns.return_value = 10
        feature_selector._to_many_features = lambda *args: False

        # act
        actual_result = feature_selector.none_reducible_valid_learnblock(
            past_n_features, lb
        )

        # assert
        assert actual_result is True

    def test_none_reducible_valid_learnblock_returns_false(self, feature_selector):
        # arrange
        feature_selector.settings.max_features = 9
        feature_selector._to_many_features = lambda *args: True
        past_n_features = 10
        lb = MagicMock(spec=PandasBlock)
        lb.n_columns.return_value = 7

        # act
        actual_result = feature_selector.none_reducible_valid_learnblock(
            past_n_features, lb
        )

        # assert
        assert actual_result is False

    def reduce_constraint_enforce_reduction_returns_true(self, feature_selector):
        # arrange/ act
        lb = MagicMock(spec=PandasBlock)
        lb.n_columns.return_value = 7

        feature_selector._to_many_features = lambda *args: True
        feature_selector.settings.max_model_reduction = False
        actual_one = feature_selector.reduce_constraint(lb)
        assert actual_one is True

        feature_selector._to_many_features = lambda *args: True
        feature_selector.settings.max_model_reduction = False
        actual_two = feature_selector.reduce_constraint(lb)
        assert actual_two is True

        feature_selector._to_many_features = lambda *args: True
        feature_selector.settings.max_model_reduction = False
        lb.n_columns.return_value = 2
        actual_three = feature_selector.reduce_constraint(lb)
        assert actual_three is True

    def reduce_constraint_enforce_reduction_returns_false(self, feature_selector):
        # arrange
        lb = MagicMock(spec=PandasBlock)
        lb.n_columns.return_value = 7
        feature_selector._to_many_features = lambda *args: False
        # act
        actual_result =  feature_selector.reduce_constraint(lb)

        # assert
        actual_result is False

    def reduce_contraint_to_many_features(self, feature_selector):
        # arrange
        lb = MagicMock(spec=PandasBlock)
        lb.n_columns.return_value = 7
        feature_selector.settings.max_model_reduction = False
        feature_selector._to_many_features = lambda *args: True

        # act
        actual_result = feature_selector.reduce_constraint(lb)

        # assert
        assert actual_result is True

    def test_reduce_filter(self, feature_selector):
        # arrange
        filtering = MagicMock()
        filtering.return_value = [20, 30]
        droping = MagicMock()

        lb = get_labeled_block()
        lb.drop_columns = droping

        feature_selector._filter_method_criteria = lambda *args: True
        feature_selector.filtering = filtering

        # act
        feature_selector.reduce(lb)

        # assert
        assert filtering.call_args[0][0] == lb
        assert droping.call_args[0][0] == [20, 30]

    def test_reduce_embedding(self, feature_selector):
        # arrange
        embedding = MagicMock()
        embedding.return_value = [20, 30]
        droping = MagicMock()

        lb = get_labeled_block()
        lb.drop_columns = droping

        feature_selector._filter_method_criteria = lambda *args: False
        feature_selector.embedding = embedding

        # act
        feature_selector.reduce(lb)

        # assert
        assert embedding.call_args[0][0] == lb
        assert droping.call_args[0][0] == [20, 30]

    def test_select_dont_enforce_reduction(self, feature_selector):
        # arrange
        lb = get_labeled_block()
        feature_selector.reduce_constraint = lambda *args: False
        feature_selector.settings.max_filter_x = 99
        feature_selector.settings.max_filter_y = 8

        # act
        log, reduced = feature_selector.select(lb)

        # assert
        assert log.state == SelectionInfo.State.REDUCIBLE
        assert log.n_rows == 90
        assert log.n_columns == 6
        assert reduced == lb

    def test_select_enforce_reduction(self, feature_selector):
        # arrange
        lb = get_labeled_block()
        lb.n_columns = lambda *a, **kw: 6
        reduce = MagicMock()

        feature_selector.reduce_constraint = lambda *args: True
        feature_selector.none_reducible_valid_learnblock = lambda *args: True
        feature_selector.reduce = reduce

        # act
        log, reduced = feature_selector.select(lb)

        # assert
        assert reduce.call_args[0][0] == lb
        assert log.state == SelectionInfo.State.REDUCIBLE
        assert log.n_rows == 90
        assert log.n_columns == 6
        assert reduced == lb

    def test_select_with_invalid_nonreducible_block(self, feature_selector):
        # arrange
        lb = get_labeled_block()
        lb.n_columns = lambda *a, **kw: 6
        reduce = MagicMock()

        feature_selector.reduce_constraint = lambda *args: True
        feature_selector.none_reducible_valid_learnblock = lambda *a: False
        feature_selector.none_reducible_invalid_learnblock = lambda *a: True
        feature_selector.reduce = reduce

        # act
        log, reduced = feature_selector.select(lb)

        # assert
        assert log.state == SelectionInfo.State.NON_REDUCIBLE
        assert reduced is None
        assert reduce.call_args[0][0] == lb


def get_ml_models():
    from sklearn.linear_model import LogisticRegression
    from sklearn.svm import SVC

    return [
        ml_adapter.ReconstructionConceptualMLModel(SVC, "Svc"),
        ml_adapter.ReconstructionConceptualMLModel(LogisticRegression, "Log"),
    ]


def test_determine_winner_with_empty_reliability_dict():
    # arrange
    win_sel = WinnerSelector()
    reliability_dict = {}

    # act
    log, winner = win_sel.determine_winner(reliability_dict, get_ml_models())

    # assert
    assert winner is None
    assert log.state == WinnerSelectionInfo.State.FAILED


def test_models_with_the_biggest_reliability():
    # arrange
    win_sel = WinnerSelector()
    reliability_dict = {0.7: ["Winners"], 0.2: [], -0.1: []}

    # act
    winners = win_sel.models_with_the_biggest_reliability(reliability_dict)

    # assert
    assert winners == ["Winners"]


def test_models_with_smallest_domain():
    # arrange
    win_sel = WinnerSelector()
    m1 = MagicMock(spec=PragmaticMachineLearningModel)
    m1.learnblock_features = [i for i in range(30)]

    m2 = MagicMock(spec=PragmaticMachineLearningModel)
    m2.learnblock_features = [i for i in range(20)]

    m3 = MagicMock(spec=PragmaticMachineLearningModel)
    m3.learnblock_features = [i for i in range(20)]

    objects = [m1, m2, m3]

    # act
    winners = win_sel.models_with_the_smallest_domain(objects)

    # assert
    assert m2 in winners
    assert m3 in winners


def test_models_with_the_biggest_accuracy():
    # arrange
    win_sel = WinnerSelector()
    m1 = MagicMock(spec=PragmaticMachineLearningModel)
    m1.accuracy = 0.01
    m1.subject = ("Tree", )
    m2 = MagicMock(spec=PragmaticMachineLearningModel)
    m2.accuracy = 0.5
    m2.subject = ("Svc", )
    m3 = MagicMock(spec=PragmaticMachineLearningModel)
    m3.accuracy = 1.0
    m3.subject = ("Gau", )
    m4 = MagicMock(spec=PragmaticMachineLearningModel)
    m4.accuracy = 1.0
    m4.subject = ("Log", )
    candidates = [m1, m2, m3, m4]

    # act
    winners = win_sel.models_with_the_biggest_accuracy(candidates)

    # assert
    assert m3 == winners[("Gau", )]
    assert m4 == winners[("Log", )]


def test_model_with_highest_prioriy_winner():
    # arrange
    win_sel = WinnerSelector()
    m1 = MagicMock(spec=PragmaticMachineLearningModel)
    m1.subject = ("Tree", )
    m2 = MagicMock(spec=PragmaticMachineLearningModel)
    m2.subject = ("Svc", )
    m3 = MagicMock(spec=PragmaticMachineLearningModel)
    m3.subject = ("Gau", )
    m4 = MagicMock(spec=PragmaticMachineLearningModel)
    m4.subject = ("Log", )
    ml_models = [m1, m1, m2, m4]
    candidates = {("Tree", ): m1, ("Svc",): m2, ("Gau",): m3, ("Log",): m4}

    # act
    winner = win_sel.model_with_highest_priority_winner(candidates, ml_models)

    # assert
    assert winner == m1


def test_model_with_highest_priority_winner_only_one():
    # arrange
    win_sel = WinnerSelector()
    m1 = MagicMock(spec=PragmaticMachineLearningModel)
    m1 = MagicMock(spec=PragmaticMachineLearningModel)
    m1.subject = ("Tree", )
    m2 = MagicMock(spec=PragmaticMachineLearningModel)
    m2.subject = ("Svc", )
    m3 = MagicMock(spec=PragmaticMachineLearningModel)
    m3.subject = ("Gau", )
    m4 = MagicMock(spec=PragmaticMachineLearningModel)
    m4.subject = ("Log", )
    ml_models = [m1, m1, m2, m4]
    candidates = {("Tree", ): m1}

    # act
    winner = win_sel.model_with_highest_priority_winner(candidates, ml_models)

    # assert
    assert m1 is winner


def test_determine_winner_success():
    # arrange
    win_sel = WinnerSelector()

    m1 = MagicMock(spec=PragmaticMachineLearningModel)
    m1 = MagicMock(spec=PragmaticMachineLearningModel)
    m1.accuracy = 0.01
    m1.subject = ("Tree", )
    m1.learnblock_features = [i for i in range(30)]
    m2 = MagicMock(spec=PragmaticMachineLearningModel)
    m2.accuracy = 0.5
    m2.subject = ("Svc", )
    m2.learnblock_features = [i for i in range(30)]
    m3 = MagicMock(spec=PragmaticMachineLearningModel)
    m3.accuracy = 1.0
    m3.subject = ("Gau", )
    m3.learnblock_features = [i for i in range(70)]
    m4 = MagicMock(spec=PragmaticMachineLearningModel)
    m4.accuracy = 1.0
    m4.subject = ("Log", )
    m4.learnblock_features = [i for i in range(30)]
    m5 = MagicMock(spec=PragmaticMachineLearningModel)
    m5.accuracy = 1.0
    m5.subject = ("For", )
    m5.learnblock_features = [i for i in range(30)]

    reliability_dict = {
        0.0: [m1], 0.2: [m2], 0.7: [m3, m4, m5]
    }

    ml_models = [m1, m2, m3, m4, m5]

    # act
    log, winner = win_sel.determine_winner(reliability_dict, ml_models)

    # assert
    assert m4 is winner
    assert log.state == WinnerSelectionInfo.State.SELECTED
