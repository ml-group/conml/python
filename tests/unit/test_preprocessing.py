from unittest.mock import MagicMock
from datetime import datetime

import pandas as pd
import pytest

from conML.domain.preprocessing import Source
from conML.domain.knowledge import KnowledgeDatabase
from conML.shared.logger import LearnblockIdentificationInfo, BlockProcessInfo
from conML.shared.settings import BlockProcessingSettings
from conML.ports.ckmeans_adapter import ckmeans
from conML.ports.ml_adapter import KernelDensityEstimator
from conML.domain.preprocessing import ClusterFinder, LearnblockIdentifier
from conML.ports.source_adapter import (
    PandasBlock,
    convert_df_to_block,
    build_block_from_rows
)


def get_labeled_block():
    df = pd.DataFrame(
        {
            "0.0.1": [pow(i, 2) for i in range(0, 100)],
            "0.0.2": [pow(i, 0.5) for i in range(0, 100)],
            "0.0.3": [pow(i, 3) for i in range(0, 100)],
            "0.0.4": [pow(i, 0.3) for i in range(0, 100)],
            "0.0.5": [pow(i, 0.1) for i in range(0, 100)],
            "0.0.6": [pow(i, 4) for i in range(0, 100)],
            "T": [0 for i in range(0, 100)],
            "Sigma": ["" for i in range(0, 100)],
            "Z": [0 if i < 50 else 1 for i in range(0, 100)]
        }
    )
    labeled = PandasBlock(df, relationship=("Z", "Sigma"), origin=("source", ))
    labeled.subject = "Kme02"
    labeled.n_cluster = 2
    labeled.origin = ("C.1.1", "C.1.2")
    return labeled


@pytest.fixture
def cluster_finder():
    mock_kernel = KernelDensityEstimator()
    settings = BlockProcessingSettings()
    mock_ckmeans = MagicMock()
    return ClusterFinder(mock_kernel, settings, mock_ckmeans)


class TestClusterFinder:

    def test_find_success(self, cluster_finder):
        # arrange
        cluster_finder.density_estimator.density = lambda *a: [0.2, 0.4, 0.1]
        cluster_finder.count_threshold_changes = lambda *a: [True, True, True]
        cluster_finder.count_changes = lambda *a: 1
        cluster_finder.ckmeans = lambda *a: [0.2, 0.4, 0.1]

        # act
        actual_result = cluster_finder.find([0.2, 0.4, 0.1])

        # assert
        assert actual_result == [0.2, 0.4, 0.1]

    def test_find_valid_learnblock_clusterself(self, cluster_finder):
        # arrange
        cluster_finder.density_estimator.density = lambda *a: [0.2, 0.4, 0.1]
        cluster_finder.count_threshold_changes = lambda *a: [True, True, True]
        cluster_finder.count_changes = lambda *a: 1
        cluster_finder.ckmeans = lambda *a: [[0.2, 0.4], [0.1]]
        cluster_finder.settings.min_category_size = 0.6

        # act
        actual_result = list(cluster_finder.find_valid_learnblock_cluster([0.2, 0.4, 0.1]))

        # assert
        assert actual_result == [[0.2, 0.4]]

    def test_count_threshold_changes(self, cluster_finder):
        # arrange
        density = [2, 7, 8, 9, 9, 10, 10, 9, 8, 1]
        cluster_finder.settings.sigma_zeta_cutoff = 0.8

        # act
        actual_result = cluster_finder.count_threshold_changes(density)

        # assert
        assert actual_result == [
            False, False, False, True, True, True, True, True, False, False,
        ]

    def test_count_changes(self, cluster_finder):
        # arrange
        chagnes_1 = [False, False, True, True, True, True, True, False, False]
        chagnes_2 = [True, False, True, True, True, True, True, False, False]
        chagnes_3 = [True, False, True, True, True, True, True, False, False]

        # act
        result_1 = cluster_finder.count_changes(chagnes_1)
        result_2 = cluster_finder.count_changes(chagnes_2)
        result_3 = cluster_finder.count_changes(chagnes_3)

        # assert
        assert result_1 == 3
        assert result_2 == 4
        assert result_3 == 4


@pytest.fixture()
def learnblock_identifier():
    settings = BlockProcessingSettings()
    kernel_mock = MagicMock(spec=KernelDensityEstimator)
    ckmeans_mock = MagicMock()

    lb_identifier = LearnblockIdentifier(
        settings, kernel_mock, ckmeans_mock
    )
    return lb_identifier


class TestLearnblockIdentifier:

    def test_column_pairs(self, learnblock_identifier):
        # arrange

        # act/assert
        it = iter(learnblock_identifier._column_pairs())
        assert next(it) == ("Sigma", "Z")
        # assert next(it) == ("T", "Sigma")
        # assert next(it) == ("T", "Z")

    def test_is_learn_block(self, learnblock_identifier):
        learnblock_identifier.settings.learn_block_minimum = 100
        actual_result = learnblock_identifier._is_learn_block(100)
        assert actual_result is True

        learnblock_identifier.settings.learn_block_minimum = 300
        actual_result = learnblock_identifier._is_learn_block(100)
        assert actual_result is False

    @pytest.mark.skip("T Sigma relatives are currenlty not searched.")
    def test_identify_relatives_t_sigma(self, learnblock_identifier):
        # arrange
        df = pd.DataFrame(
            {
                "0.0.1": [i for i in range(10)],
                "T": [1, 1, 1, 2, 1, 1, 2, 3, 4, 5],
                "Sigma": ["", "", "", "X", "", "", "Y", "", "", ""],
                 "Z":    ["", "", "", "", "", "", "", "", "", ""]
            }
        )
        lb = PandasBlock(df, origin=("source",))

        # act
        it = iter(learnblock_identifier._identify_relatives(lb, *("Sigma", "Z")))
        res_one = next(it)
        res_two = next(it)

        # assert
        assert list(res_one.df["0.0.1"]) == [0, 1, 2, 4, 5]
        assert list(res_two.df["0.0.1"]) == [7, 8, 9]

    def test_identify_relatives_sigma_z(self, learnblock_identifier):
        # arrange
        df = pd.DataFrame(
            {
                "0.0.1": [i for i in range(10)],
                "T": [1, 1, 1, 2, 1, 1, 2, 3, 4, 5],
                "Sigma": ["", "", "", "X", "Y", "X", "Y", "", "", ""],
                "Z":     ["", "", "", "", "", "", "", "", "", ""]
            }
        )
        lb = PandasBlock(df, origin=("source",))

        # act
        res = next(iter(learnblock_identifier._identify_relatives(lb, *("T", "Sigma"))))

        # assert
        assert list(res.df["0.0.1"]) == [0, 1, 2]

    def test_identify_relatives_t_z(self, learnblock_identifier):
        # arrange
        df = pd.DataFrame(
            {
                "0.0.1": [i for i in range(10)],
                "T": [1, 1, 1, 2, 1, 1, 2, 3, 4, 5],
                "Sigma": ["", "", "", "X", "Y", "X", "Y", "", "", ""],
                "Z": ["", "", "", "", "", "", "", "", "", ""]
            }
        )
        lb = PandasBlock(df, origin=("source",))

        # act
        res = next(iter(learnblock_identifier._identify_relatives(lb, *("T", "Z"))))

        # assert
        assert list(res.df["0.0.1"]) == [0, 1, 2, 4, 5]

    def test_get_sigma_zeta_relatives(self, learnblock_identifier):
        # arrange
        expected_result = MagicMock(spec=PandasBlock)

        pb = MagicMock(spec=PandasBlock)
        relative = MagicMock(spec=PandasBlock)
        relative.new_block_from.return_value = expected_result
        pb = pb.construct_from_two_pairs.return_value = relative
        learnblock_identifier.cluster_finder.find = lambda *a: [[0, 1, 2]]


        # act
        actual_result = list(
            learnblock_identifier._get_sigma_zeta_relatives(
                pb, Z=" ", Sigma=" "))

        # assert
        assert actual_result[0] is expected_result

    def test_get_halde_returns_whole_block(self, learnblock_identifier):
        # arrange
        df = pd.DataFrame(
            {
                "0.0.1": [i for i in range(10)],
                "T": [1, 1, 1, 2, 1, 1, 2, 3, 4, 5],
                "Sigma": ["", "", "", "X", "Y", "X", "Y", "", "", ""],
                "Z": ["", "", "", "", "", "", "", "", "", ""]
            }
        )
        lb = PandasBlock(df, origin=("source",))

        # act
        actual_result = learnblock_identifier.get_halde(learnblock=None, block=lb)

        # assert
        assert actual_result.equals(df)

    def test_get_halde_returns_learnblock(self, learnblock_identifier):
        # arrange
        df = pd.DataFrame(
            {
                "0.0.1": [i for i in range(10)],
                "T": [1, 1, 1, 2, 1, 1, 2, 3, 4, 5],
                "Sigma": ["", "", "", "X", "Y", "X", "Y", "", "", ""],
                "Z": ["", "", "", "", "", "", "", "", "", ""]
            }
        )
        block = PandasBlock(df, origin=("source",))
        lb_df = df.iloc[0:3, 0:4]
        lb = PandasBlock(lb_df, origin=("source", ))

        # act
        actual_result = learnblock_identifier.get_halde(lb, block)

        # assert
        assert list(actual_result["T"]) == [2, 1, 1, 2, 3, 4, 5]
        assert list(actual_result["0.0.1"]) == [3, 4, 5, 6, 7, 8, 9]
        assert list(actual_result["Sigma"]) == ["X", "Y", "X", "Y", "", "", ""]
        assert list(actual_result["Z"]) == ["", "", "", "", "", "", ""]

    def test_identify_valid_lb(self, learnblock_identifier):
        # arrange
        block = MagicMock(spec=PandasBlock)
        valid_lb = MagicMock(spec=PandasBlock)
        valid_lb.n_rows.return_value = 100
        valid_lb.n_columns.return_value = 10
        valid_lb.relationship = ("Z", "Sigma")
        halde = pd.DataFrame()

        learnblock_identifier._identify_relatives = lambda *args: [valid_lb]
        learnblock_identifier._is_learn_block = lambda *args: True
        learnblock_identifier.get_halde = lambda *args: halde

        # act
        actual_result = learnblock_identifier.identify(block)

        # assert
        assert actual_result[0].state == LearnblockIdentificationInfo.State.IDENTIFIED
        assert actual_result[1] is valid_lb
        assert actual_result[2] is halde

    def test_identify_not_any_lb(self, learnblock_identifier):
        # arrange
        block = MagicMock(spec=PandasBlock)
        halde = pd.DataFrame()

        learnblock_identifier._identify_relatives = lambda *args: []
        learnblock_identifier._is_learn_block = lambda *args: True
        learnblock_identifier.get_halde = lambda *args: halde

        # act
        actual_result = learnblock_identifier.identify(block)

        # assert
        assert actual_result[0].state == LearnblockIdentificationInfo.State.DISCARDED
        assert actual_result[1] is None
        assert actual_result[2] is halde


@pytest.fixture()
def source():
    knowledge_mock = MagicMock(spec=KnowledgeDatabase)
    mock_convert_df_to_block = MagicMock()
    return Source(knowledge_mock, mock_convert_df_to_block)


class TestSource:

    def test_process(self, source):
        # arrange
        pd_block = MagicMock(spec=PandasBlock)
        pd_block.n_rows.return_value = 100
        pd_block.n_columns.return_value = 10
        source.convert_to_pandas_block = lambda *a: pd_block
        df = pd.DataFrame()

        # act
        info, pandas_block = source.process(df)

        # assert
        assert info.n_rows == 100
        assert info.n_columns == 10
        assert pandas_block is pd_block

    def test_convert_to_pandas(self, source):
        # arrange
        df = pd.DataFrame()
        pd_block = MagicMock(spec=PandasBlock)
        source.df_converter = lambda *args: pd_block

        # act
        pandas_block = source.convert_to_pandas_block(df)

        # assert
        assert pandas_block is pd_block

    def test_append_to_knowledge(self, source):
        # arrange
        pd_block = MagicMock(spec=PandasBlock)
        source.knowledge.extend = lambda *a: 20

        # act
        source.append_to_knowledge(pd_block)

        # assert
        assert pd_block.origin == (20, )