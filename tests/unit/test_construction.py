import time
from random import randint
from math import sin
from unittest.mock import patch, MagicMock

import pytest
import pandas as pd
from sklearn.cluster import KMeans
from sklearn.cluster.spectral import SpectralClustering
from sklearn.cluster.k_means_ import KMeans

from conML.domain.construction.construction import return_model, Constructor
from conML.ports.ml_adapter import (
    ConstructionClusteringMLModel,
    MachineLearningModel)
from conML.shared.logger import ConstructionInfo
from conML.shared.settings import ConstructionSettings
from conML.ports.source_adapter import PandasBlock


@pytest.fixture()
def conceptual_constructor():
    mode = "conceptual"
    settings = ConstructionSettings()
    algorithms = [
        ConstructionClusteringMLModel(KMeans(), "Kme"),
        ConstructionClusteringMLModel(SpectralClustering(), "Spe")
    ]

    return Constructor(mode, algorithms, settings)


@patch("conML.domain.construction.Constructor")
def test_constructor_calling_constructor(mock_constructor):
    # arrage
    mode = "conceptual"
    ml_models = [MagicMock(spec=MachineLearningModel),
                 MagicMock(spec=MachineLearningModel)]
    settings = ConstructionSettings()

    # act
    Constructor(mode, ml_models, settings)

    # assert
    assert mock_constructor.called_with(mode, ml_models, settings)


def test_constructor_proper_initialization():
    # arrange
    mode = "conceptual"
    ml_models = [MagicMock(spec=MachineLearningModel()),
                 MagicMock(spec=MachineLearningModel())]

    settings = ConstructionSettings()

    # act
    constructor = Constructor(mode, ml_models, settings)

    # assert
    assert constructor.mode == "conceptual"
    assert constructor.ml_models == ml_models


def test_constructor_change_settings_by_users():
    # arrange
    mode = "conceptual"
    ml_models = [MachineLearningModel(), MachineLearningModel()]
    settings = ConstructionSettings()
    categorical_complexity = 33
    min_category_size = 0.1

    # act
    constructor = Constructor(mode, ml_models, settings)
    constructor.max_categories = categorical_complexity
    constructor.min_category_size = min_category_size

    # assert
    assert constructor.max_categories == categorical_complexity
    assert constructor.min_category_size == min_category_size


def learnblock():
    return PandasBlock(
        relationship=("Z", "Sigma"),
        origin=(1, ),
        dataframe=pd.DataFrame({
            "0.0.1": [sin(randint(0, 10)) for _ in range(100)],
            "0.0.2": [sin(randint(0, 20)) for _ in range(100)],
            "0.0.3": [sin(randint(0, 30)) for _ in range(100)],
            "0.0.4": [sin(randint(0, 100)) for _ in range(100)],
            "0.0.5": [sin(randint(0, 200)) for _ in range(100)],
            "T": [int(time.time()) for _ in range(100)],
            "Sigma": [0 for _ in range(100)],
            "Z": ["" for _ in range(100)]},
        )
    )


def test_serial_constructor_success(conceptual_constructor):
    # arrange
    lb = learnblock()

    m1 = MagicMock(spec=ConstructionClusteringMLModel)
    m1.found_cluster = 2
    m1.abbreviation = "Kme02"

    m2 = MagicMock(spec=ConstructionClusteringMLModel)
    m2.found_cluster = 4
    m2.abbreviation = "Kme04"

    m3 = MagicMock(spec=ConstructionClusteringMLModel)
    m3.found_cluster = 2
    m3.abbreviation = "Spe02"

    m4 = MagicMock(spec=ConstructionClusteringMLModel)
    m4.found_cluster = 4
    m4.abbreviation = "Spe04"

    m1.train = lambda *args: None
    m2.train = lambda *args: None
    m3.train = lambda *args: None
    m4.trian = lambda *args: None

    conceptual_constructor.max_categories = 3
    conceptual_constructor.machine_learning_models = lambda *args: [m1, m2, m3, m4]
    conceptual_constructor.valid_trained = lambda *args: True
    conceptual_constructor.label_block = lambda *args: lb

    # act
    constructed_lbs = list(conceptual_constructor.construct(lb))

    # assert
    subjects = (i for i in ["Kme02", "Kme04", "Spe02", "Spe04"])
    cluster = (i for i in [2, 4, 2, 4])

    for info, constructed_lb in constructed_lbs:
        assert info.state == ConstructionInfo.State.CONSTRUCTED
        assert info.subject == next(subjects)
        assert info.n_cluster == next(cluster)
        assert constructed_lb == lb


@patch("conML.ports.source_adapter.PandasBlock.labeled", lambda *args: True)
def test_serial_constructor_with_labeled_learnblock(conceptual_constructor):
    # arrange
    lb = learnblock()
    constructor = Constructor("conceptual",
                              [ConstructionClusteringMLModel(KMeans, "Kme")],
                              ConstructionSettings)

    # act
    actual_result = list(constructor.construct(lb)).pop()

    # assert
    assert actual_result[0].state == ConstructionInfo.State.PASSED
    assert actual_result[1] == lb


def test_serial_constructor_machine_learning_models(conceptual_constructor):
    # arrange
    m1 = MagicMock(spec=ConstructionClusteringMLModel)
    m1.cluster_specific = True
    m1.cluster = 2
    m1.abbreviation = "Kme"

    m2 = MagicMock(spec=ConstructionClusteringMLModel)
    m2.cluster_specific = True
    m2.cluster = 2
    m2.abbreviation = "Kme"

    m3 = MagicMock(spec=ConstructionClusteringMLModel)
    m3.cluster_specific = True
    m3.cluster = 3
    m3.abbreviation = "Kme"

    m1.new_model_depending_on_cluster.side_effect = [m2, m3]

    conceptual_constructor.max_categories = 3
    conceptual_constructor.ml_models = [m1]

    # act
    result = list(conceptual_constructor.machine_learning_models())

    # assert
    assert result[0].cluster == 2
    assert result[0].abbreviation == "Kme"

    assert result[1].cluster == 3
    assert result[1].abbreviation == "Kme"


def test_valid_trained_success(conceptual_constructor):
    # arrange
    conceptual_constructor.min_category_size = 0.1
    mock_model = MagicMock(spec=ConstructionClusteringMLModel)
    mock_model.found_cluster = 2
    mock_model.cluster_sizes = {"0": 10, "1": 19}

    # act
    actual_response = conceptual_constructor.valid_trained(mock_model, 29)

    # assert
    assert actual_response is True


def test_valid_trained_failure(conceptual_constructor):
    # arrange
    conceptual_constructor.min_category_size = 0.1
    mock_model = MagicMock(spec=ConstructionClusteringMLModel)
    mock_model.found_cluster = 2
    mock_model.cluster_sizes = {"0": 10, "1": 5}

    # act
    actual_response = conceptual_constructor.valid_trained(mock_model, 100)

    # assert
    assert actual_response is False


def test_label_block(conceptual_constructor):
    # arrange
    lb = learnblock()
    mock_model = MagicMock(spec=ConstructionClusteringMLModel)
    labels = [randint(1, 2) for _ in range(100)]
    mock_model.get_labels.return_value = labels
    mock_model.found_cluster = 2
    mock_model.abbreviation = "Kme"

    # act
    actual_labeled_lb = conceptual_constructor.label_block(lb, mock_model)

    # assert
    assert actual_labeled_lb.subject == "Kme02"
    assert actual_labeled_lb.get_column("Z") == labels
    assert actual_labeled_lb.origin == (1, )
    assert actual_labeled_lb.n_cluster == 2


def test_return_model():
    # arrange
    m = MagicMock(spec=MachineLearningModel)

    # act
    actual_result = next(iter(return_model(m)))

    # assert
    assert actual_result is m


def test_str_and_repr_constructor(conceptual_constructor):
    # act
    str_rep = str(conceptual_constructor)
    repr_rep = repr(conceptual_constructor)

    # assert
    assert str_rep == (
        "\n".join([
            "{:20}: {}".format("Mode", "conceptual"),
            "{:20}: {}".format("ML Models", [
                str(m) for m in conceptual_constructor.ml_models]),
            str(conceptual_constructor.settings)
        ])
    )

    assert repr_rep == (
        ", ".join([
        "{}={}".format("mode", "conceptual"),
        "{}".format(repr(conceptual_constructor.settings)),
        "{}={}".format("ml_models", repr(conceptual_constructor.ml_models))
    ]))


def test_no_valid_construction(conceptual_constructor):
    # arrange
    conceptual_constructor.valid_trained = lambda *args: False
    lb = learnblock()

    # act
    log, constructed = next(iter(conceptual_constructor.construct(lb)))

    # assert
    assert log.state == ConstructionInfo.State.DISCARDED
    assert log.subject == "Kme"
    assert log.n_cluster == 2
    assert constructed is None


def test_valid_trained_returns_false(conceptual_constructor):
    # arrange
    m = MagicMock(spec=ConstructionClusteringMLModel)
    m.found_cluster = 1

    # act
    actual_result = conceptual_constructor.valid_trained(m, 100)

    # assert
    assert actual_result is False


def test_serial_constructor_per_combination(conceptual_constructor):
    # arrange
    conceptual_constructor.settings.max_categories = 3

    # act
    actual_result = (
        list(conceptual_constructor.serial_constructor_per_combination()))

    # assert
    assert len(actual_result) == 4
    assert next(iter(actual_result[0].machine_learning_models())).cluster == 2
    assert next(iter(actual_result[1].machine_learning_models())).cluster == 3
    assert next(iter(actual_result[2].machine_learning_models())).cluster == 2
    assert next(iter(actual_result[3].machine_learning_models())).cluster == 3
