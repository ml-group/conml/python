import os

from sklearn import cluster
from sklearn import feature_selection
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.naive_bayes import GaussianNB

import conML
from conML.domain.knowledge import KnowledgeDatabase
from conML.ports.ml_adapter import MachineLearningModel
from conML.ports.source_adapter import convert_df_to_block
from conML.shared import settings
from conML.domain.deconstruction.deconstruction import (
    SerialDeconstructor, ParallelDeconstructor)


def test_constructino_command():
    # arrange
    algos = [("Agg", cluster.AgglomerativeClustering()),
             ("Kme", cluster.KMeans())]

    # act
    actual_result = conML.construction("conceptual", algos)

    # assert
    assert len(actual_result.ml_models) == 2
    assert actual_result.mode == "conceptual"
    assert [isinstance(m, MachineLearningModel) for m in actual_result.ml_models]
    assert actual_result.settings == settings.ConstructionSettings()


def test_reconstruction_command():
    # arrange
    algos = [("Gau", GaussianNB()), ("Tre", ExtraTreesClassifier())]

    # act
    actual_result = conML.reconstruction("conceptual", algos)

    # assert
    assert len(actual_result.ml_models) == 2
    assert actual_result.mode == "conceptual"
    assert [isinstance(m, MachineLearningModel) for m in actual_result.ml_models]
    assert actual_result.settings == settings.ReconstructionSettings()


def test_feature_selection_command():
    # arrange
    embedded_method = feature_selection.SelectFromModel(
        ExtraTreesClassifier(n_estimators=200)
    )
    filter_method = feature_selection.VarianceThreshold(200)

    # act
    actual_result = conML.feature_selection(filter_method=filter_method,
                                            embedded_method=embedded_method)

    # assert
    assert isinstance(actual_result.filter_method, MachineLearningModel)
    assert actual_result.settings == settings.FeatureSelectionSettings()


def test_deconstruction_command():
    # arrange
    algos = [("Gau", GaussianNB()), ("Tre", ExtraTreesClassifier())]
    reconstructor = conML.reconstruction("conceptual", algos)

    # act
    deconstructor = conML.deconstruction(reconstructor)

    # assert
    assert isinstance(deconstructor.state, SerialDeconstructor)


def test_load_knowledge():
    # arrange

    database = KnowledgeDatabase(7, convert_df_to_block)
    database.save("knowledge.bin")

    # act
    actual_result = conML.load_knowledge("knowledge.bin")
    os.remove("knowledge.bin")


    # assert
    assert database.highest_level == actual_result.highest_level
    assert database.n_models == actual_result.n_models
