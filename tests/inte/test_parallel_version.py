import os

import pandas as pd
import pytest

from sklearn import cluster
from sklearn import feature_selection
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC

import conML
settings = conML.get_settings()
settings.HIGHEST_LEVEL = 5

settings.LEARN_BLOCK_MINIMUM = 50
settings.SIGMA_ZETA_CUTOFF = 0.2

settings.MAX_CATEGORIES = 5
settings.MIN_CATEGORY_SIZE = 0.1

settings.MIN_BUILD_MODELS = 2
settings.MAX_FEATURES = 5
settings.MAX_FILTER_X = 50
settings.MAX_FILTER_Y = 1500
settings.MAX_MODELS_REDUCTION = False

settings.MIN_TEST_ACCURACY = 0.8
settings.RELIABILITY_SAMPLE = 0.3
settings.MIN_RELIABILITY = 0.8
settings.REDUCE_MODEL_REDUNDANCY = False

settings.DECONST_STRATEGY = "integrative"
settings.DECONST_MODE = "minimal"
settings.DECONST_MAX_DISTANCE_T = 1.0
settings.DECONST_FULL_TOLERANCE = 0.1
settings.FORCE_TIME_EXPANSION = True
settings.ALLOW_WEAK_RELIABILITY = True


def create_components(con_algos, recon_algos, feat_selct_algos):
    constructor = conML.construction("conceptual", con_algos)
    selector = conML.feature_selection(**feat_selct_algos)
    reconstructor = conML.reconstruction("conceptual", recon_algos)
    deconstructor = conML.deconstruction(reconstructor)
    return constructor, selector, reconstructor, deconstructor


def test_knowledge_search_parallel_with_parallel_deconstructor():
    # arrange
    block_size = 100
    feature_size = 10
    settings.SIGMA_ZETA_CUTOFF = 0.01

    algos = (
        [("Kme", cluster.KMeans()), ("Agg", cluster.AgglomerativeClustering())],
        [("Svc", SVC()), ("Nei", KNeighborsClassifier())],
        {
            "embedded_method": feature_selection.SelectFromModel(
                ExtraTreesClassifier(n_estimators=200)
            ),
            "filter_method": feature_selection.VarianceThreshold(2000)

        }
    )

    def generate_blocks():
        path = os.path.join("../../conML/static/toyset.csv")
        df = pd.read_csv(path, index_col=False)
        df["Z"] = ""

        for start in range(0, df.shape[0], block_size):
            bl = df.iloc[start:start+block_size, 0:feature_size]
            bl["T"], bl["Sigma"], bl["Z"] = df["T"], df["Sigma"], df["Z"]
            yield bl

        for start in range(0, df.shape[0], block_size):
            bl = df.iloc[start:start + block_size, feature_size:feature_size*2]
            bl["T"], bl["Sigma"], bl["Z"] = df["T"], df["Sigma"], df["Z"]
            yield bl

    components = create_components(*algos)
    components[0].max_categories = 3
    components[3].transit()
    components[3].deconst_mode = "full"
    components[3].deconst_strategy = "oppurtunistic"

    # act
    dbs, halden = [], []
    with conML.knowledge_searcher(*components, n_procs=2) as searcher:
        for block in generate_blocks():
            db, halde = searcher.search(block)
            dbs.append(db)
            halden.append(halde)

    # assert
    assert len(halden) == 60


def test_knowledge_search_parallel_get_halde_dbs():
    # arrange
    block_size = 500
    feature_size = 100
    settings.SIGMA_ZETA_CUTOFF = 0.01

    algos = (
        [("Kme", cluster.KMeans()), ("Agg", cluster.AgglomerativeClustering())],
        [("Svc", SVC()), ("Nei", KNeighborsClassifier())],
        {
            "embedded_method": feature_selection.SelectFromModel(
                ExtraTreesClassifier(n_estimators=200)
            ),
            "filter_method": feature_selection.VarianceThreshold(2000)

        }
    )

    def generate_blocks():
        path = os.path.join("../../conML/static/toyset.csv")
        df = pd.read_csv(path, index_col=False)
        df["Z"] = ""

        for start in range(0, df.shape[0], block_size):
            bl = df.iloc[start:start+block_size, 0:feature_size]
            bl["T"], bl["Sigma"], bl["Z"] = df["T"], df["Sigma"], df["Z"]
            yield bl

    components = create_components(*algos)
    components[0].max_categories = 3

    # act
    dbs, halden = [], []
    with conML.knowledge_searcher(*components, n_procs=2) as searcher:
        for block in generate_blocks():
            db, halde = searcher.search(block)
            dbs.append(db)
            halden.append(halde)

    # assert
    assert len(halden) == 6
    assert all([isinstance(h, pd.DataFrame) for h in halden])


def test_knowledge_search_parallel_different_block_sizes():
    # arrange
    feature_size = 5

    algos = (
        [("Kme", cluster.KMeans()), ("Agg", cluster.AgglomerativeClustering())],
        [("Svc", SVC()), ("Nei", KNeighborsClassifier())],
        {
            "embedded_method": feature_selection.SelectFromModel(
                ExtraTreesClassifier(n_estimators=200)
            ),
            "filter_method": feature_selection.VarianceThreshold(2000)

        }
    )

    def generate_blocks():
        path = os.path.join("../../conML/static/toyset.csv")
        df = pd.read_csv(path, index_col=False)
        df["Z"] = ""

        for start in range(0, df.shape[0], 250):
            bl = df.iloc[start:start+250, 0:feature_size]
            bl["T"], bl["Sigma"], bl["Z"] = df["T"], df["Sigma"], df["Z"]
            yield bl

        for start in range(0, df.shape[0], 500):
            bl = df.iloc[start:start+250, 0:feature_size]
            bl["T"], bl["Sigma"], bl["Z"] = df["T"], df["Sigma"], df["Z"]
            yield bl

    components = create_components(*algos)
    components[0].max_categories = 2

    # act
    dbs, halden = [], []
    with conML.knowledge_searcher(*components, n_procs=3) as searcher:
        for i, block in enumerate(generate_blocks()):
            db, halde = searcher.search(block)
            dbs.append(db)
            halden.append(halde)

    # assert
    assert dbs[0].source.n_block == 1
    assert dbs[len(dbs)-1].source.n_block == 18


def test_knowledge_search_different_feature_sizes():
    # arrange
    settings.LEARN_BLOCK_MINIMUM = 100
    settings.SIGMA_ZETA_CUTOFF = 0.01
    settings.MAX_FEATURES = 5

    algos = (
        [("Kme", cluster.KMeans()), ("Agg", cluster.AgglomerativeClustering())],
        [("Gau", GaussianNB()), ("Nei", KNeighborsClassifier())],
        {
            "embedded_method": feature_selection.SelectFromModel(
                ExtraTreesClassifier(n_estimators=200)
            ),
            "filter_method": feature_selection.VarianceThreshold(2000)

        }
    )

    def generate_blocks():
        path = os.path.join("../../conML/static/toyset.csv")
        df = pd.read_csv(path, index_col=False)
        df["Z"] = ""

        for start in range(0, df.shape[0], 250):
            bl = df.iloc[start:start+250, 0:10]
            bl["T"], bl["Sigma"], bl["Z"] = df["T"], df["Sigma"], df["Z"]
            yield bl

        for start in range(0, df.shape[0], 500):
            bl = df.iloc[start:start+250, 10:15]
            bl["T"], bl["Sigma"], bl["Z"] = df["T"], df["Sigma"], df["Z"]
            yield bl

    components = create_components(*algos)
    components[0].max_categories = 5

    # act
    dbs, halden = [], []
    with conML.knowledge_searcher(*components, n_procs=4) as searcher:
        for block in generate_blocks():
            try:
                db, halde = searcher.search(block)
                dbs.append(db)
                halden.append(halde)

            except Exception:
                assert False


def test_knowledge_search_different_feature_sizes_raises_exception():
    # arrange
    settings.LEARN_BLOCK_MINIMUM = 100
    settings.SIGMA_ZETA_CUTOFF = 0.01
    settings.MAX_FEATURES = 5

    algos = (
        [("Kme", cluster.KMeans()), ("Agg", cluster.AgglomerativeClustering())],
        [("Gau", GaussianNB()), ("Nei", KNeighborsClassifier())],
        {
            "embedded_method": feature_selection.SelectFromModel(
                ExtraTreesClassifier(n_estimators=200)
            ),
            "filter_method": feature_selection.VarianceThreshold(2000)

        }
    )

    def generate_blocks():
        path = os.path.join("../../conML/static/toyset.csv")
        df = pd.read_csv(path, index_col=False)
        df["Z"] = ""

        for start in range(0, df.shape[0], 250):
            bl = df.iloc[start:start+250, 0:10]
            bl["T"], bl["Sigma"], bl["Z"] = df["T"], df["Sigma"], df["Z"]
            yield 1, bl

        for start in range(0, df.shape[0], 500):
            bl = df.iloc[start:start+250, 23:22]
            bl["T"], bl["Sigma"], bl["Z"] = df["T"], df["Sigma"], df["Z"]
            yield 2, bl

    # act
    components = create_components(*algos)
    components[0].max_categories = 5

    # assert
    dbs, halden = [], []
    with conML.knowledge_searcher(*components, n_procs=5) as searcher:
        for i, block in generate_blocks():
            if i == 1:
                db, halde = searcher.search(block)
                dbs.append(db)
                halden.append(halde)

            if i == 2:
                with pytest.raises(Exception):
                    db, halde = searcher.search(block)
                    dbs.append(db)
                    halden.append(halde)


def test_knowledge_search_using_existing_database():
    # arrange
    settings.LEARN_BLOCK_MINIMUM = 100
    settings.SIGMA_ZETA_CUTOFF = 0.01
    settings.MAX_FEATURES = 5

    algos = (
        [("Kme", cluster.KMeans()), ("Agg", cluster.AgglomerativeClustering())],
        [("Gau", GaussianNB()), ("Nei", KNeighborsClassifier())],
        {
            "embedded_method": feature_selection.SelectFromModel(
                ExtraTreesClassifier(n_estimators=200)
            ),
            "filter_method": feature_selection.VarianceThreshold(2000)

        }
    )

    def generate_blocks():
        path = os.path.join("../../conML/static/toyset.csv")
        df = pd.read_csv(path, index_col=False)
        df["Z"] = ""

        for start in range(0, df.shape[0], 250):
            bl = df.iloc[start:start+250, 0:10]
            bl["T"], bl["Sigma"], bl["Z"] = df["T"], df["Sigma"], df["Z"]
            yield bl

        for start in range(0, df.shape[0], 500):
            bl = df.iloc[start:start+250, 12:24]
            bl["T"], bl["Sigma"], bl["Z"] = df["T"], df["Sigma"], df["Z"]
            yield bl

    components = create_components(*algos)
    components[0].max_categories = 5

    # act
    dbs, halden = [], []
    with conML.knowledge_searcher(*components, n_procs=6) as searcher:
        for block in generate_blocks():
            db, halde = searcher.search(block)
            dbs.append(db)
            halden.append(halde)

    # assert
    with conML.knowledge_searcher(
            *components, knowledge_database=dbs[-1]
    ) as searcher:
        for block in generate_blocks():
            try:
                db, halde = searcher.search(block)
                dbs.append(db)
                halden.append(halde)

            except Exception:
                assert False
